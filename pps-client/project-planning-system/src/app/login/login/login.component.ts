import { Component, OnInit } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AppState } from 'src/app/state/app-state';
import { loginUserRequest } from 'src/app/auth/store/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  formControl = '';
  hide = true;

  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {

  }

  public getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  public login(email: string, password: string) {
    const user: any = { email, password };
    this.store.dispatch(loginUserRequest({ user }));
  }

}
