import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageComponent } from './homepage/homepage/homepage.component';
import { LoginComponent } from './login/login/login.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomepageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', loadChildren: () => import('./users/users.module').then(el => el.UsersModule), canActivate: [AuthGuard] },
  { path: 'projects', loadChildren: () => import('./projects/projects.module').then(el => el.ProjectsModule), canActivate: [AuthGuard] },
  { path: 'skills', loadChildren: () => import('./shared/skills/skills.module').then(el => el.SkillsModule), canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    { onSameUrlNavigation: 'reload' }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
