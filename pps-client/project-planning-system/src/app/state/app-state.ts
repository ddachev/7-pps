import { User } from '../models/users/user.model';
import { Project } from '../models/projects/project.model';
import { Skill } from '../models/skills/skill.model';

export interface AppState {
  users: User[];
  user: User;
  projects: Project[];
  project: Project;
  skills: Skill[];
  auth: {
    isLoggedIn: boolean;
    loggedUserId: number;
  };
  loggedUser: User;
  myProjects: Project[];
}
