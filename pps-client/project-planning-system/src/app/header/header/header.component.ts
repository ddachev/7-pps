import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from 'src/app/state/app-state';
import { userLogoutRequest } from 'src/app/auth/store/auth.actions';
import { User } from 'src/app/models/users/user.model';
import { clearLoggedUserRequest } from 'src/app/users/store/all-users.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() sidenavToggle = new EventEmitter<void>();

  loggedIn$ = this.store.pipe(select(state => state.auth.isLoggedIn));
  loggedUser$: Observable<User> = this.store.pipe(select(state => state.loggedUser));

  loggedUser: User;

  constructor(
    private readonly store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    this.loggedUser$.subscribe(user => {
      this.loggedUser = user;
    });
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  public logout() {
    this.store.dispatch(clearLoggedUserRequest());
    this.store.dispatch(userLogoutRequest());
  }

}
