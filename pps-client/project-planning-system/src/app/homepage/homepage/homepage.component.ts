import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  slides = [
    { img: 'https://i.ibb.co/t20L29C/111.jpg' },
    { img: 'https://i.ibb.co/w7g8Jtx/123456.png' },
    { img: 'https://i.ibb.co/dKc4h1d/ijflbszdflskj.png' },
    { img: 'https://i.ibb.co/w7g8Jtx/123456.png' }
  ];

  slideConfig = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'dots': true,
    'infinite': true,
    'autoplay': true,
    'autoplaySpeed': 2500
  };
  constructor() { }

  ngOnInit(): void {
  }

  addSlide() {
    this.slides.push({ img: 'http://placehold.it/350x150/777777' });
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    console.log('afterChange');
  }

  beforeChange(e) {
    console.log('beforeChange');
  }
}

