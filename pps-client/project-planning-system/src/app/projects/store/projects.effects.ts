import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProjectsDataService } from '../project-data-service.service';

import { switchMap, map } from 'rxjs/operators';
import {
  refreshProjectsRequest,
  refreshProjectsDone,
  refreshMyProjectsRequest,
  refreshMyProjectsDone,
  createProjectRequest,
  refreshProjectByIdRequest,
  refreshProjectByIdDone,
  stopProjectByIdRequest} from './projects.actions';

@Injectable()
export class ProjectsEffects {

  constructor(
    private readonly projectsService: ProjectsDataService,
    private readonly actions$: Actions
  ) { }

  refreshProjects$ = createEffect(() => this.actions$.pipe(
    ofType(refreshProjectsRequest),
    switchMap(() => {
      return this.projectsService.getAllProjects().pipe(
        map(projects => {
          return refreshProjectsDone({ projects });
        })
      );
    })
  ));

  refreshMyProjects$ = createEffect(() => this.actions$.pipe(
    ofType(refreshMyProjectsRequest),
    switchMap(() => {
      return this.projectsService.getAllMyProjects().pipe(
        map(myProjects => {
          return refreshMyProjectsDone({ myProjects });
        })
      );
    })
  ));

  createProject$ = createEffect(() => this.actions$.pipe(
    ofType(createProjectRequest),
    switchMap((action) => {
      return this.projectsService.createProject(action.project).pipe(
        map(() => refreshProjectsRequest())
      );
    })
  ));

  getProjectById$ = createEffect(() => this.actions$.pipe(
    ofType(refreshProjectByIdRequest),
    switchMap((action) => {
      return this.projectsService.getProjectById(action.projectId).pipe(
        map(project => refreshProjectByIdDone({ project }))
      );
    })
  ));

  stopProjectById$ = createEffect(() => this.actions$.pipe(
    ofType(stopProjectByIdRequest),
    switchMap((action) => {
      return this.projectsService.stopProjectById(action.projectId).pipe(
        map(() => refreshProjectByIdRequest({ projectId: action.projectId }))
      );
    })
  ));

}
