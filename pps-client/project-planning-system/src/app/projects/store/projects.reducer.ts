import { refreshProjectsDone, refreshProjectByIdDone, refreshMyProjectsDone } from './projects.actions';
import { createReducer, on } from '@ngrx/store';
import { Project } from 'src/app/models/projects/project.model';

export const projectsReducer = createReducer<Project[]>([],
  on(refreshProjectsDone, (_, action) => action.projects));

export const myProjectsReducer = createReducer<Project[]>([],
    on(refreshMyProjectsDone, (_, action) => action.myProjects));

export const projectReducer = createReducer<Project>(null,
    on(refreshProjectByIdDone, (_, action) => action.project));
