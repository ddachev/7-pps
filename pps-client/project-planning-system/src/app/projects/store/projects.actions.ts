import { createAction, props } from '@ngrx/store';
import { Project } from 'src/app/models/projects/project.model';

export const refreshProjectsRequest = createAction('[Project] Refresh Projects Request');
export const refreshProjectsDone = createAction('[Project] Refresh Projects Done', props<{ projects: Project[] }>());
export const createProjectRequest = createAction('[Project] Create Project Request', props<{ project: Project, manager: {
  isLoggedIn: boolean,
  loggedUserId: number
} }>());
export const refreshProjectByIdRequest = createAction('[Project] Refresh Project By Id Request', props<{ projectId: number }>());
export const refreshProjectByIdDone = createAction('[Project] Refresh Project By Id Done', props<{ project: Project }>());

export const refreshMyProjectsRequest = createAction('[Project] Refresh My Projects Request');
export const refreshMyProjectsDone = createAction('[Project] Refresh My Projects Done', props<{ myProjects: Project[] }>());

export const stopProjectByIdRequest = createAction('[Project] Stop Project By Id Request', props<{ projectId: number }>());
