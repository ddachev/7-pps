import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {
  FormGroup,
  Validators,
  FormArray,
  FormBuilder,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

import { AllProjectsComponent } from '../all-projects/all-projects.component';
import { Project } from 'src/app/models/projects/project.model';
import { Skill } from 'src/app/models/skills/skill.model';
import { AppState } from 'src/app/state/app-state';
import { refreshSkillsRequest } from 'src/app/shared/store/shared.actions';
import { User } from 'src/app/models/users/user.model';
import { refreshUsersRequest } from 'src/app/users/store/all-users.actions';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { refreshProjectsRequest } from '../store/projects.actions';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css'],
})
export class CreateProjectComponent implements OnInit {
  @Input() searchSkill = [];
  @Input() taskHoursForUser = 0;
  @Output() createdProject: EventEmitter<Project> = new EventEmitter<Project>();

  fb = new FormBuilder();
  form: FormGroup;
  titlesToCompare = [];
  temporaryResources = [];
  temporaryUsers = [];
  isSubmitted = false;
  minDate;

  skills$: Observable<Skill[]> = this.store.pipe(
    select((state) => state.skills)
  );
  users$: Observable<User[]> = this.store.pipe(select((state) => state.users));
  projects$: Observable<Project[]> = this.store.pipe(select(state => {
    return state.projects;
  }));


  constructor(
    private store: Store<AppState>,
    public dialogRef: MatDialogRef<AllProjectsComponent>,
    private formBuilder: FormBuilder,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit(): void {
    this.minDate = new Date();

    this.form = this.fb.group({
      title: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(20),
      ]),
      description: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),

      endDate: this.fb.control(null, [Validators.required]),
      resources: this.fb.array([this.createResource()]),
    });

    this.store.dispatch(refreshSkillsRequest());
    this.store.dispatch(refreshUsersRequest());
    this.store.dispatch(refreshProjectsRequest());

    this.users$.subscribe((users) => {
      this.temporaryUsers = users;
    });

    this.projects$.subscribe(projects => {
      this.titlesToCompare = projects.map(project => project.title);
    });
  }

  createResource(): FormGroup {
    return this.formBuilder.group({
      skillId: '',
      hours: '',
      tasks: this.fb.array([this.createTask()]),
    });
  }

  createTask(): FormGroup {
    return this.formBuilder.group({
      userId: null,
      hours: null,
    });
  }

  get resources() {
    return this.form.get('resources') as FormArray;
  }

  addResource() {
    this.resources.push(this.createResource());
  }

  get tasks() {
    return this.form.get('resources.tasks') as FormArray;
  }

  addTask(i) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const control = (<FormArray> this.form.controls.resources)
      .at(i)
      .get('tasks') as FormArray;
    control.push(this.createTask());
  }

  removeTask(i, ix, task) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const control = (<FormArray> this.form.controls.resources)
      .at(i)
      .get('tasks') as FormArray;
    control.removeAt(ix);

    const foundUserTask = this.temporaryResources.find(
      (resource) => resource.userId === task.value.userId
    );

    foundUserTask.hours = foundUserTask.hours - task.value.hours;
  }

  removeResource(resource, i) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const resourcesArray = this.form.controls.resources as FormArray;
    resourcesArray.removeAt(i);

    const userIds = resource.value.tasks.map((task) => task.userId);

    const tasksToDelete = this.temporaryResources.map((temporaryTask) => {
      if (userIds.includes(temporaryTask.userId)) {
        const deletedTask = resource.value.tasks.find(
          (task) => task.userId === temporaryTask.userId
        );
        temporaryTask.hours = temporaryTask.hours - deletedTask.hours;
      }
    });

  }

  createProject() {
    const newProject = this.form.value;

    const resources = newProject.resources.reduce((previous, current) => {
      const resource = {
        hours: current.hours,
        skillId: current.skillId,
      };
      previous.push(resource);

      return previous;
    }, []);

    const tasks = newProject.resources.reduce((acc, resource) => {
      const resourceSkill = resource.skillId;

      // tslint:disable-next-line: no-shadowed-variable
      const tasks = resource.tasks.map((t) => {
        return {
          hours: +t.hours,
          skillId: resourceSkill,
          userId: t.userId,
        };
      });
      acc.push(...tasks);
      return acc;
    }, []);

    const returnProject = {
      ...newProject,
      resources,
      tasks,
    };

    this.createdProject.emit(returnProject);
    this.notificator.success('You have successfully created a new project.');
  }


  setSkillForFilter(skillId: number, i: number) {
    if (!this.searchSkill[i]) {
      this.searchSkill.push(skillId);
    } else if (this.searchSkill[i] !== skillId) {
      this.searchSkill[i] = skillId;
    } else {
      return;
    }
  }

  addUserToTiming(task) {
    if (task.value.hours === null) {
      task.value.hours = 0;
    }
    const existingUsers = this.temporaryResources.map(
      (resource) => resource.userId
    );

    if (existingUsers.includes(task.value.userId)) {
      return;
    } else {
      this.temporaryResources.push(task.value);
    }

  }

  addTaskHoursToTiming(task) {
    const foundTask = this.temporaryResources.find(
      (resource) => resource.userId === task.value.userId
    );
    foundTask.hours = foundTask.hours + task.value.hours;
  }


  availabilityCalculating(user) {
    if (this.temporaryResources.length > 0) {

      const foundTask = this.temporaryResources.find(
        (resource) => resource.userId === user.id
      );

      if (foundTask) {
        return foundTask.hours;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }


  findUser(userId: number): User {
    const foundUser = this.temporaryUsers.find(user => user.id === userId);

    return foundUser;
  }
}
