import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material.module';
import { ProjectsDataService } from './project-data-service.service';
import { AllProjectsComponent } from './all-projects/all-projects.component';
import { SingleProjectComponent } from './single-project/single-project.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { UserSkillPipe } from './pipes/user-skill.pipe';
import { MyProjectsComponent } from './my-projects/my-projects.component';
import { UserAvailabilityPipe } from './pipes/user-availability.pipe';


@NgModule({
  declarations: [
    AllProjectsComponent,
    SingleProjectComponent,
    CreateProjectComponent,
    MyProjectsComponent,
    UserSkillPipe,
    UserAvailabilityPipe
  ],
  providers: [ProjectsDataService],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ProjectsRoutingModule,
    FlexLayoutModule,
  ],
  exports: [
    MyProjectsComponent,
    AllProjectsComponent,
    UserSkillPipe,
    UserAvailabilityPipe
  ]
})
export class ProjectsModule { }
