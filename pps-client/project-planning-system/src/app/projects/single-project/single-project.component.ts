import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/state/app-state';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { Project } from 'src/app/models/projects/project.model';
import { refreshProjectByIdRequest, stopProjectByIdRequest } from '../store/projects.actions';
import { Resource } from 'src/app/models/resources/resource.model';
import { CreateTaskComponent } from 'src/app/shared/tasks/components/create-task/create-task.component';
import { addTasksToProjectRequest, addResourceToProjectRequest, updateResourceTimeRequest } from 'src/app/shared/store/shared.actions';
import { refreshLoggedUserByIdRequest } from 'src/app/users/store/all-users.actions';
import { CreateResourceComponent } from 'src/app/shared/resources/components/create-resource/create-resource.component';
import { CoreService } from 'src/app/core/services/core.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.css']
})
export class SingleProjectComponent implements OnInit {

  project: Project;
  timeLeftPerResource: number;
  panelOpenState = true;
  resourceHours = new FormControl('');
  showUpdateResource: { [key: number]: boolean } = {};
  onTime;

  authUser$: Observable<{
    isLoggedIn: boolean;
    loggedUserId: number;
  }> = this.store.pipe(select((state) => state.auth));

  project$: Observable<Project> = this.store.pipe(select(state => {
    return state.project;
  }));

  constructor(
    private store: Store<AppState>,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    public dialog: MatDialog,
    public coreService: CoreService,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit(): void {
    this.authUser$.subscribe(authData => {
      if (authData) {
        this.store.dispatch(refreshLoggedUserByIdRequest({ userId: Number(authData.loggedUserId) }));
      }
    });

    this.store.dispatch(refreshProjectByIdRequest({ projectId: Number(this.route.snapshot.params.id) }));

    this.project$.subscribe(data => {
      if (data) {
        this.project = data;
      }
    });

    this.changeClass();
  }

  calculateTime(resource: Resource) {
    const tasks = this.project.tasks.filter(task => task.skill.name === resource.skill.name && task.isDeleted === false);
    const tasksHours = tasks.reduce((sum, task) => {
      sum += task.hours;
      return sum;
    }, 0);

    const tasksDeleted = this.project.tasks.filter(task => task.skill.name === resource.skill.name && task.isDeleted === true);
    const deletedTaskHours = tasksDeleted.reduce((sum, task) => {
      sum += task.hours;
      return sum;
    }, 0);

    const totalDaysNeededPerResource = resource.hours / tasksHours;
    const daysPassedSinceProjectStart = ((new Date().getTime() - new Date(this.project.startDate).getTime()) / (60 * 60 * 24 * 1000));

    const workDoneInDaysForDeletedTasks = tasksDeleted.reduce((sumInDays, task) => {
      // tslint:disable-next-line: max-line-length
      let daysPassedUntilTaskDeleted = new Date(task.deleteDate).getTime() - new Date(task.startDate).getTime();

      if (daysPassedUntilTaskDeleted > 0) {
        daysPassedUntilTaskDeleted = daysPassedUntilTaskDeleted / (60 * 60 * 24 * 1000);
      } else {
        daysPassedUntilTaskDeleted = 1;
      }

      const workDoneInDays = +((daysPassedUntilTaskDeleted * task.hours) / 24).toFixed(2);
      const workDoneInHours = workDoneInDays / task.hours;

      sumInDays += workDoneInDays;
      return +sumInDays.toFixed(2);
    }, 0);

    // tslint:disable-next-line: max-line-length
    const leftTimeInDaysPerResource = +(totalDaysNeededPerResource - daysPassedSinceProjectStart - workDoneInDaysForDeletedTasks).toFixed(2);
    // console.log(totalDaysNeededPerResource);
    const leftTimeInHoursPerResource = leftTimeInDaysPerResource * tasksHours;

    return Math.ceil(leftTimeInHoursPerResource);
  }


  openTaskDialog(resource: any) {
    const dialogRef = this.dialog.open(CreateTaskComponent, {
      disableClose: true,
      data: resource
    });
    const subscribeTask = dialogRef.componentInstance.createdTask.subscribe(tasks => {
      this.doCreateTask(tasks, this.project.id);
    });


    dialogRef.afterClosed().subscribe(() => {
      subscribeTask.unsubscribe();
    });
  }


  doCreateTask(tasks: any, projectId: number) {
    this.store.dispatch(addTasksToProjectRequest({ tasks, projectId }));
  }


  openResourceDialog() {
    const dialogRef = this.dialog.open(CreateResourceComponent, {
      disableClose: true,
      data: this.project
    });
    const subscribeResource = dialogRef.componentInstance.createdResource.subscribe(createResourceBody => {
      this.doCreateResource(this.project.id, createResourceBody);
    });

    dialogRef.afterClosed().subscribe(() => {
      subscribeResource.unsubscribe();
    });
  }


  doCreateResource(projectId: number, createResourceBody: any) {
    this.store.dispatch(addResourceToProjectRequest({ projectId, createResourceBody }));
  }

  openShowUpdateResource(index: number) {
    this.showUpdateResource[index] = true;
  }

  closeShowUpdateResource(index: number) {
    this.showUpdateResource[index] = false;
  }

  updateResourceTime(projectId: number, resourceId: number, hours: number, index: number) {
    this.store.dispatch(updateResourceTimeRequest({ projectId, resourceId, hours }));
    this.resourceHours.setValue('');
    this.closeShowUpdateResource(index);
  }

  stopProject() {
    this.store.dispatch(stopProjectByIdRequest({ projectId: this.project.id }));
    this.notificator.warn('You have successfully stopped the project.');
    setTimeout(() => this.router.navigate(['projects']), 500);
  }

  changeClass() {
    if (this.project.endDate) {
      if (this.coreService.calculateEndDate(this.project).getTime() < new Date(this.project.endDate).getTime()) {
        this.onTime = true;
      } else {
        this.onTime = false;
      }
    }
  }

}
