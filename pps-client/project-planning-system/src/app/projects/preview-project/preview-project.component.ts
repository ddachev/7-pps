import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-preview-project',
  templateUrl: './preview-project.component.html',
  styleUrls: ['./preview-project.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PreviewProjectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
