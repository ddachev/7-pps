import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app-state';
import { MatDialog } from '@angular/material/dialog';

import { Project } from 'src/app/models/projects/project.model';
import { refreshMyProjectsRequest, createProjectRequest } from '../store/projects.actions';
import { User } from 'src/app/models/users/user.model';
import { CreateProjectComponent } from '../create-project/create-project.component';

@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  styleUrls: ['./my-projects.component.css']
})
export class MyProjectsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['title', 'status', 'capacity', 'startDate', 'deadline', 'ETC'];
  dataSource = new MatTableDataSource<Project>();
  projects: Project[];
  projectManager = null;
  loggedUser: User;

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  projects$: Observable<Project[]> = this.store.pipe(select(state => {
    return state.myProjects;
  }));
  authUser$: Observable<{
    isLoggedIn: boolean;
    loggedUserId: number;
  }> = this.store.pipe(select((state) => state.auth));

  loggedUser$: Observable<User> = this.store.pipe(select(state => state.loggedUser));


  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog
  ) { }


  ngOnInit(): void {
    this.store.dispatch(refreshMyProjectsRequest());

    this.projects$.subscribe((projects: Project[]) => {
      this.dataSource.data = projects;
    });

    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'capacity': return item.tasks.length;
        case 'startDate': return item.startDate;
        case 'deadline': return item.endDate;
        default: return item[property];
      }
    };

    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };

    this.authUser$.subscribe(authData => {
      this.projectManager = authData;
    });

    this.loggedUser$.subscribe(user => {
      this.loggedUser = user;
    });
  }


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(searchStr: string) {
    this.dataSource.filter = searchStr.trim().toLowerCase();
  }

  openDialog() {
    const dialogRef = this.dialog.open(CreateProjectComponent, {
      disableClose: true
    });
    const subscribeProject = dialogRef.componentInstance.createdProject.subscribe(project => {
      this.doCreateProject(project, this.projectManager);

    });

    dialogRef.afterClosed().subscribe(() => {
      subscribeProject.unsubscribe();
    });
  }

  doCreateProject(project: Project, manager) {
    this.store.dispatch(createProjectRequest({ project, manager }));
  }

}
