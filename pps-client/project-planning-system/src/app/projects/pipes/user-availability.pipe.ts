import { PipeTransform, Pipe } from '@angular/core';

import { User } from 'src/app/models/users/user.model';

@Pipe({
  name: 'filterUserByAvailability'
})
export class UserAvailabilityPipe implements PipeTransform {

  transform(users: User[]): User[] {

    return users.filter(user => {
      return user.availability > 0;
    });
  }
}
