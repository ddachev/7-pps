import { PipeTransform, Pipe } from '@angular/core';

import { User } from 'src/app/models/users/user.model';

@Pipe({
  name: 'filterUserBySkill'
})
export class UserSkillPipe implements PipeTransform {

  transform(users: User[], skillId: number): User[] {
    if (!skillId) {
      return users;
    }

    return users.filter(user => {
      return user.skills.map(skill => skill.id).includes(skillId);
    });
  }

}
