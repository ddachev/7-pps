import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/state/app-state';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';

import { Project } from 'src/app/models/projects/project.model';
import { refreshProjectsRequest, createProjectRequest } from '../store/projects.actions';
import { CreateProjectComponent } from '../create-project/create-project.component';
import { User } from 'src/app/models/users/user.model';
import { refreshLoggedUserByIdRequest } from 'src/app/users/store/all-users.actions';
import { CoreService } from 'src/app/core/services/core.service';

@Component({
  selector: 'app-all-projects',
  templateUrl: './all-projects.component.html',
  styleUrls: ['./all-projects.component.css']
})
export class AllProjectsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['title', 'manager', 'status', 'capacity', 'startDate', 'deadline', 'ETC'];
  dataSource = new MatTableDataSource<Project>();
  projects: Project[];
  projectManager = null;
  loggedUser: User;

  loggedUserId$: Observable<number> = this.store.pipe(select(state => state.auth.loggedUserId));
  loggedUser$: Observable<User> = this.store.pipe(select(state => state.loggedUser));

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  projects$: Observable<Project[]> = this.store.pipe(select(state => {
    return state.projects;
  }));

  authUser$: Observable<{
    isLoggedIn: boolean;
    loggedUserId: number;
  }> = this.store.pipe(select((state) => state.auth));

  constructor(
    private store: Store<AppState>,
    public coreService: CoreService,
    public dialog: MatDialog
  ) { }


  ngOnInit(): void {
    this.projects$.subscribe((projects: Project[]) => {
      this.dataSource.data = projects;
    });

    this.authUser$.subscribe(authData => {
      if (authData) {
        this.projectManager = authData;
        this.store.dispatch(refreshLoggedUserByIdRequest({ userId: Number(authData.loggedUserId) }));
      }
    });

    this.loggedUser$.subscribe(user => {
      this.loggedUser = user;
    });

    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'manager': return item.manager.firstName;
        case 'capacity': return item.tasks.length;
        case 'startDate': return item.startDate;
        case 'deadline': return item.endDate;
        default: return item[property];
      }
    };

    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };

    this.store.dispatch(refreshProjectsRequest());
  }


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(searchStr: string) {
    this.dataSource.filter = searchStr.trim().toLowerCase();
  }

  openDialog() {
    const dialogRef = this.dialog.open(CreateProjectComponent, {
      disableClose: true
    });
    const subscribeProject = dialogRef.componentInstance.createdProject.subscribe(project => {
      this.doCreateProject(project, this.projectManager);

    });

    dialogRef.afterClosed().subscribe(() => {
      subscribeProject.unsubscribe();
    });
  }

  doCreateProject(project: Project, manager) {
    this.store.dispatch(createProjectRequest({ project, manager }));
  }
}
