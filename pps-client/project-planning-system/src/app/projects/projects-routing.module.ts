import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { SingleProjectComponent } from './single-project/single-project.component';
import { PreviewProjectComponent } from './preview-project/preview-project.component';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  { path: '', component: PreviewProjectComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'projects/:id', component: SingleProjectComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ProjectsRoutingModule { }
