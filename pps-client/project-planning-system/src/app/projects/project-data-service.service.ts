import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Project } from '../models/projects/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectsDataService {

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  getAllProjects(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(`http://localhost:3000/projects`);
  }

  getAllMyProjects(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(`http://localhost:3000/projects/my`);
  }

  createProject(project: Project): Observable<Project> {
    return this.httpClient.post<Project>(`http://localhost:3000/projects`, project);
  }

  getProjectById(projectId: number): Observable<Project> {
    return this.httpClient.get<Project>(`http://localhost:3000/projects/${projectId}`);
  }

  stopProjectById(projectId: number): Observable<void> {
    return this.httpClient.put<any>(`http://localhost:3000/projects/${projectId}`, null);
  }
}
