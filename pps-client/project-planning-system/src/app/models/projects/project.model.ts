import { User } from '../users/user.model';
import { Resource } from '../resources/resource.model';
import { Task } from '../tasks/task';

export interface Project {
  id?: number;
  title?: string;
  description?: string;
  startDate?: string;
  endDate?: string;
  status?: string;
  manager?: User;
  resources?: Resource[];
  tasks?: Task[];
}
