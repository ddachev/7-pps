export interface CreateProjectDTO {
  title: string;
  description: string;
  endDate: string;
  managerId: number;
  resources: any[];
  tasks: any[];
}
