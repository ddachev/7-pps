import { Project } from '../projects/project.model';
import { Skill } from '../skills/skill.model';
import { User } from '../users/user.model';

export class Task {
  user?: User;
  project?: Project;
  hours?: number;
  startDate?: Date;
  deleteDate?: Date;
  skill?: Skill;
  isDeleted?: boolean;
}
