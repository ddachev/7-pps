import { Skill } from '../skills/skill.model';

export interface Resource {
  id?: number;
  hours?: number;
  skill?: Skill;
  isDeleted?: boolean;
}
