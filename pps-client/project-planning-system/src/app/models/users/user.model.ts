import { Skill } from '../skills/skill.model';
import { Task } from '../tasks/task';
import { Project } from '../projects/project.model';

export class User {
  id?: number;
  email?: string;
  firstName?: string;
  lastName?: string;
  position?: string;
  role?: string;
  availability?: number;
  manager?: User;
  skills?: Skill[];
  tasks?: Task[];
  subordinates?: User[];
  managedProjects?: Project[];
}
