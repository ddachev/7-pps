import { Injectable } from '@angular/core';
import { Project } from 'src/app/models/projects/project.model';
import { Task } from 'src/app/models/tasks/task';
import { Resource } from 'src/app/models/resources/resource.model';

@Injectable({
  providedIn: 'root'
})

export class CoreService {

  constructor() { }

  calculateEndDate(project: Project): Date {

    const existingResources = project.resources.filter(resource => resource.isDeleted === false);

    const timePerResources: number[] = existingResources.reduce((acc, resource: Resource) => {
      const currentTasks: Task[] = project.tasks.filter(t => {
        if (t.isDeleted === false && t.skill.name === resource.skill.name) {
          return true;
        }
      });

      const deletedTasks: Task[] = project.tasks.filter(t => {
        if (t.isDeleted === true && t.skill.name === resource.skill.name) {
          return true;
        }
      });

      // Total workhours per day for a single resource
      const tasksDailyInputSum: number = currentTasks.reduce((sum, task) => {
        sum += task.hours;
        return sum;
      }, 0);

      // DAYS
      const daysPassedSinceProjectStart: number = ((new Date().getTime() - new Date(project.startDate).getTime()) / (60 * 60 * 24 * 1000));
      const daysNeededPerResource: number = resource.hours / tasksDailyInputSum;
      const workDoneForTasks: number = deletedTasks.reduce((sum, task) => {

        const daysPassed = ((new Date(task.deleteDate).getTime() - new Date(task.startDate).getTime()) / (60 * 60 * 24 * 1000));
        const workDoneInDays = ((daysPassed * task.hours) / 24);

        sum += workDoneInDays;
        return sum;
      }, 0);

      const totalTimeInDaysPerResource = (daysNeededPerResource - daysPassedSinceProjectStart);

      acc.push(totalTimeInDaysPerResource);

      return acc;
    }, []);

    const longestTimePerResource = Math.round(Math.max(...timePerResources));

    if (longestTimePerResource < 1) {
      const currDate = new Date();
      return currDate;
    }

    const date = new Date(project.startDate);

    return new Date(date.setDate(date.getDate() + longestTimePerResource));
  }

}
