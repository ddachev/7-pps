import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AuthGuard } from './auth/auth.guard';
import { allUsersReducer, userReducer, loggedUserReducer } from './users/store/all-users.reducer';
import { AllUsersEffects } from './users/store/all-users.effects';
import { PreviewProjectComponent } from './projects/preview-project/preview-project.component';
import { HomepageComponent } from './homepage/homepage/homepage.component';
import { HeaderComponent } from './header/header/header.component';
import { CreateTaskComponent } from './shared/tasks/components/create-task/create-task.component';
import { CreateResourceComponent } from './shared/resources/components/create-resource/create-resource.component';
import { LoginComponent } from './login/login/login.component';
import { NavigationComponent } from './navigation/navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found/not-found.component';
import { projectsReducer, projectReducer, myProjectsReducer } from './projects/store/projects.reducer';
import { ProjectsEffects } from './projects/store/projects.effects';
import { SkillsEffects } from './shared/store/shared.effects';
import { skillsReducer } from './shared/store/shared.reducer';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { ProjectsModule } from './projects/projects.module';
import { UsersModule } from './users/users.module';
import { AuthEffects } from './auth/store/auth.effects';
import { authReducer } from './auth/store/auth.reducers';
import { TokenInterceptorService } from './core/services/token-interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    PreviewProjectComponent,
    HomepageComponent,
    HeaderComponent,
    CreateTaskComponent,
    CreateResourceComponent,
    LoginComponent,
    NavigationComponent,
    NotFoundComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    ProjectsModule,
    UsersModule,
    FlexLayoutModule,
    FormsModule,
    SlickCarouselModule,
    StoreModule.forRoot({
      users: allUsersReducer,
      projects: projectsReducer,
      skills: skillsReducer,
      auth: authReducer,
      project: projectReducer,
      user: userReducer,
      loggedUser: loggedUserReducer,
      myProjects: myProjectsReducer
    }),
    StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([AllUsersEffects, ProjectsEffects, SkillsEffects, AuthEffects]),
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
