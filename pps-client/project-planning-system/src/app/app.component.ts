import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { autoLoginUserRequest } from './auth/store/auth.actions';
import { AppState } from './state/app-state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'project-planning-system';
  loggedUserId$: Observable<number> = this.store.pipe(select(state => state.auth.loggedUserId));

  constructor(private readonly store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(autoLoginUserRequest());
  }

}
