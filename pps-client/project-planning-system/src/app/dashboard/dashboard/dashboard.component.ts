import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/state/app-state';
import { refreshLoggedUserByIdRequest } from 'src/app/users/store/all-users.actions';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/users/user.model';
import { Project } from 'src/app/models/projects/project.model';
import { MatTableDataSource } from '@angular/material/table';
import { refreshMyProjectsRequest } from 'src/app/projects/store/projects.actions';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CoreService } from 'src/app/core/services/core.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  panelOpenState1 = false;
  panelOpenState2 = false;

  displayedColumns = ['title', 'management', 'deadline', 'etc'];
  dataSource = new MatTableDataSource<Project>();
  projects: Project[];
  loggedUser: User;

  @ViewChild('sort', { static: false }) sort: MatSort;
  @ViewChild('paginator', { static: false }) paginator: MatPaginator;

  loggedUserId$: Observable<number> = this.store.pipe(select(state => state.auth.loggedUserId));
  loggedUser$: Observable<User> = this.store.pipe(select(state => state.loggedUser));
  projects$: Observable<Project[]> = this.store.pipe(select(state => {
    return state.myProjects;
  }));

  constructor(
    private readonly store: Store<AppState>,
    public coreService: CoreService,
  ) { }

  ngOnInit(): void {
    this.store.dispatch(refreshMyProjectsRequest());

    this.loggedUserId$.subscribe(userId => {
      this.store.dispatch(refreshLoggedUserByIdRequest({ userId: Number(userId) }));
    });

    this.loggedUser$.subscribe(user => {
      if (user) {
        this.loggedUser = user;
      }
    });

    this.loggedUserId$.subscribe(id => {
      if (!id) {
        this.loggedUser$ = null;
      }
    });

    this.projects$.subscribe((projects: Project[]) => {
      const inProgressProjects = projects.filter(p => p.status === 'in-progress');
      this.dataSource.data = inProgressProjects;
    });

    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };

    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'deadline': return item.endDate;
        case 'management': return item.tasks[0].hours;
        default: return item[property];
      }
    };

  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(searchStr: string) {
    this.dataSource.filter = searchStr.trim().toLowerCase();
  }

}
