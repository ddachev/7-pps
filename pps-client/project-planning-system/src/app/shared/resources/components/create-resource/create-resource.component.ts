import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

import { Skill } from 'src/app/models/skills/skill.model';
import { User } from 'src/app/models/users/user.model';
import { AppState } from 'src/app/state/app-state';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { refreshSkillsRequest } from 'src/app/shared/store/shared.actions';
import { refreshUsersRequest } from 'src/app/users/store/all-users.actions';
import { SingleProjectComponent } from 'src/app/projects/single-project/single-project.component';

@Component({
  selector: 'app-create-resource',
  templateUrl: './create-resource.component.html',
  styleUrls: ['./create-resource.component.css']
})
export class CreateResourceComponent implements OnInit {

  fb = new FormBuilder();
  form: FormGroup;

  searchSkill = null;
  temporaryResources = [];

  @Input() taskHoursForUser = 0;
  @Output() createdResource: EventEmitter<any> = new EventEmitter<any>();

  skills$: Observable<Skill[]> = this.store.pipe(select((state) => state.skills));
  users$: Observable<User[]> = this.store.pipe(select((state) => state.users));


  constructor(
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<SingleProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }


  ngOnInit(): void {
    this.form = this.fb.group({
      skillId: '',
      hours: '',
      tasks: this.fb.array([this.createTask()])
    });

    this.store.dispatch(refreshSkillsRequest());
    this.store.dispatch(refreshUsersRequest());
  }


  createTask(): FormGroup {
    return this.formBuilder.group({
      userId: null,
      hours: null,
    });
  }


  get tasks() {
    return this.form.get('tasks') as FormArray;
  }


  addTask() {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const control = (<FormArray> this.form.controls.resources).get('tasks') as FormArray;
    control.push(this.createTask());
  }


  removeTask(i, task) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const control = (<FormArray> this.form.controls.resources).get('tasks') as FormArray;
    control.removeAt(i);
    const foundUserTask = this.temporaryResources
      .find(resource => resource.userId === task.value.userId);

    foundUserTask.hours = foundUserTask.hours - task.value.hours;
  }


  onCreateResource() {
    const newResource = this.form.value;

    const tasks = newResource.tasks.map(task => {
      return {
        hours: +task.hours,
        skillId: this.form.value.skillId,
        userId: task.userId
      };
    });
    const returnResource = {
      skillId: this.form.value.skillId,
      hours: this.form.value.hours,
      tasks,
    };
    this.createdResource.emit(returnResource);
  }


  addUserToTiming(task) {
    if (task.value.hours === null) {
      task.value.hours = 0;
    }
    const existingUsers = this.temporaryResources.map(resource => resource.userId);
    if (existingUsers.includes(task.value.userId)) {
      return;
    } else {
      this.temporaryResources.push(task.value);
    }
  }


  addTaskHoursToTiming(task) {
    const foundTask = this.temporaryResources
      .find(resource => resource.userId === task.value.userId);

    foundTask.hours = foundTask.hours + task.value.hours;
  }


  availabilityCalculating(user) {
    if (this.temporaryResources.length > 0) {
      const foundTask = this.temporaryResources
        .find(resource => resource.userId === user.id);
      if (foundTask) {
        return foundTask.hours;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }
}
