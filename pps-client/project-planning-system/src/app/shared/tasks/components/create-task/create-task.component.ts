import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { AppState } from 'src/app/state/app-state';
import { SingleProjectComponent } from 'src/app/projects/single-project/single-project.component';
import { refreshUsersRequest } from 'src/app/users/store/all-users.actions';
import { User } from 'src/app/models/users/user.model';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {

  form: FormGroup;
  fb = new FormBuilder();

  @Output() createdTask: EventEmitter<any> = new EventEmitter<any>();

  users$: Observable<User[]> = this.store.pipe(select((state) => state.users));

  constructor(
    private store: Store<AppState>,
    public dialogRef: MatDialogRef<SingleProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group( {
      userId: this.fb.control(null, [Validators.required]),
      hours: this.fb.control('', [Validators.required, Validators.min(1), Validators.max(8)])
    });

    this.store.dispatch(refreshUsersRequest());
  }

  createTask() {
    const newTask = {
      tasks: [{
        hours: this.form.value.hours,
        skillId: this.data.skill.id,
        userId: this.form.value.userId
      }]
    };

    this.createdTask.emit(newTask);
  }
}
