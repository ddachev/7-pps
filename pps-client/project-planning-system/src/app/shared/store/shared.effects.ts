import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';
import { SharedDataService } from '../shared-data.service';
import { refreshSkillsDone, refreshSkillsRequest, createSkillRequest, addTasksToProjectRequest, addResourceToProjectRequest, updateResourceTimeRequest, addSkillToUserRequest } from './shared.actions';
import { refreshProjectsDone, refreshProjectByIdDone, refreshProjectByIdRequest } from 'src/app/projects/store/projects.actions';
import { refreshUserByIdRequest } from 'src/app/users/store/all-users.actions';

@Injectable()
export class SkillsEffects {

  constructor(
    private readonly sharedService: SharedDataService,
    private readonly actions$: Actions
  ) { }

  refreshSkills$ = createEffect(() => this.actions$.pipe(
    ofType(refreshSkillsRequest),
    switchMap(() => {
      return this.sharedService.getAllSkills().pipe(
        map(skills => {
          return refreshSkillsDone({ skills });
        })
      );
    })
  ));

  createSkill$ = createEffect(() => this.actions$.pipe(
    ofType(createSkillRequest),
    switchMap((action) => {
      return this.sharedService.createSkill(action.skill).pipe(
        map(() => refreshSkillsRequest())
      );
    })
  ));

  addSkillToUser$ = createEffect(() => this.actions$.pipe(
    ofType(addSkillToUserRequest),
    switchMap((action) => {
      return this.sharedService.addSkillToUser(action.userId, action.skillId).pipe(
        map(() => refreshUserByIdRequest({ userId: action.userId }))
      );
    })
  ));

  addTask$ = createEffect(() => this.actions$.pipe(
    ofType(addTasksToProjectRequest),
    switchMap((action) => {
      return this.sharedService.addTaskToProject(action.tasks, action.projectId).pipe(
        map(() => refreshProjectByIdRequest({ projectId: action.projectId }))
      );
    })
  ));

  addResource$ = createEffect(() => this.actions$.pipe(
    ofType(addResourceToProjectRequest),
    switchMap((action) => {
      return this.sharedService.addResourceToProject(action.projectId, action.createResourceBody ).pipe(
        map(() => refreshProjectByIdRequest({ projectId: action.projectId }))
      );
    })
  ));

  updateResourceTime$ = createEffect(() => this.actions$.pipe(
    ofType(updateResourceTimeRequest),
    switchMap((action) => {
      return this.sharedService.updateResourceTime(action.projectId, action.resourceId, action.hours ).pipe(
        map(() => refreshProjectByIdRequest({ projectId: action.projectId }))
      );
    })
  ));

}
