import { createReducer, on } from '@ngrx/store';

import { Skill } from 'src/app/models/skills/skill.model';
import { refreshSkillsDone } from './shared.actions';


export const skillsReducer = createReducer<Skill[]>([],
  on(refreshSkillsDone, (_, action) => action.skills));
