import { createAction, props } from '@ngrx/store';
import { Skill } from 'src/app/models/skills/skill.model';
import { Task } from 'src/app/models/tasks/task';

export const refreshSkillsRequest = createAction('[Skill] Refresh Skills Request');
export const refreshSkillsDone = createAction('[Skill] Refresh Skills Done', props<{ skills: Skill[] }>());
export const createSkillRequest = createAction('[Skill] Create Skills Request', props<{ skill: Skill }>());

export const addTasksToProjectRequest = createAction('[Task] Add Tasks Request', props<{ tasks: any, projectId: number }>());
export const addSkillToUserRequest = createAction('[Skill] Add Skill Request', props<{ userId: number, skillId: number }>());
// tslint:disable-next-line: max-line-length
export const addResourceToProjectRequest = createAction('[Resource] Add Resource Request', props<{ projectId: number, createResourceBody: any }>());
export const updateResourceTimeRequest = createAction('[Resource] Update Resource Time Request',
  props<{
    projectId: number,
    resourceId: number,
    hours: number
  }>());


