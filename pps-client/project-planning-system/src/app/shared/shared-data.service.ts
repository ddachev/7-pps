import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Skill } from '../models/skills/skill.model';
import { Project } from '../models/projects/project.model';
import { Resource } from '../models/resources/resource.model';
import { User } from '../models/users/user.model';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  getAllSkills(): Observable<Skill[]> {
    return this.httpClient.get<Skill[]>(`http://localhost:3000/skills`);
  }

  createSkill(skill: Skill): Observable<Skill> {
    return this.httpClient.post<Skill>(`http://localhost:3000/skills`, skill);
  }

  addTaskToProject(tasks: any, projectId: number): Observable<Project> {
    return this.httpClient.post<any>(`http://localhost:3000/projects/${projectId}/tasks`, tasks);
  }

  addResourceToProject(projectId: number, createResourceBody: any): Observable<Project> {
    return this.httpClient.post<any>(`http://localhost:3000/projects/${projectId}/resource`, createResourceBody);
  }

  updateResourceTime(projectId: number, resourceId: number, hours: number): Observable<Resource> {
    return this.httpClient.put<Resource>(`http://localhost:3000/projects/${projectId}/resources/${resourceId}`, {hours});
  }

  addSkillToUser(userId: number, skillId: number): Observable<User> {
    return this.httpClient.put<User>(`http://localhost:3000/users/${userId}/skills/${skillId}`, null);
  }
}
