import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedDataService } from '../shared-data.service';
import { AllSkillsComponent } from './components/all-skills/all-skills.component';
import { CreateSkillComponent } from './components/create-skill/create-skill.component';
import { MaterialModule } from 'src/app/material.module';
import { SkillsRoutingModule } from './skills-routing.module';

@NgModule({
  declarations: [AllSkillsComponent, CreateSkillComponent],
  providers: [SharedDataService],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SkillsRoutingModule
  ],
  exports: []
})
export class SkillsModule { }
