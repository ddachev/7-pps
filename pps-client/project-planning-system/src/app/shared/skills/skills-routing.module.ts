import { Routes, RouterModule } from '@angular/router';
import { AllSkillsComponent } from './components/all-skills/all-skills.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/auth/auth.guard';

const routes: Routes = [
  { path: '', component: AllSkillsComponent, pathMatch: 'full', canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SkillsRoutingModule { }
