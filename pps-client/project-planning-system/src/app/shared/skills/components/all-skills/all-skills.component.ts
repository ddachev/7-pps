import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Skill } from 'src/app/models/skills/skill.model';
import { Store, select } from '@ngrx/store';
import { MatSort } from '@angular/material/sort';
import { AppState } from 'src/app/state/app-state';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';

import { refreshSkillsRequest, createSkillRequest } from 'src/app/shared/store/shared.actions';
import { CreateSkillComponent } from '../create-skill/create-skill.component';
import { refreshLoggedUserByIdRequest } from 'src/app/users/store/all-users.actions';

@Component({
  selector: 'app-all-skills',
  templateUrl: './all-skills.component.html',
  styleUrls: ['./all-skills.component.css']
})
export class AllSkillsComponent implements OnInit, AfterViewInit {

  displayedColumns = ['id', 'name'];
  dataSource = new MatTableDataSource<Skill>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  skills$: Observable<Skill[]> = this.store.pipe(select(state => state.skills));

  authUser$: Observable<{
    isLoggedIn: boolean;
    loggedUserId: number;
  }> = this.store.pipe(select((state) => state.auth));

  constructor(
    private readonly store: Store<AppState>,
    public dialog: MatDialog) { }



  ngOnInit(): void {

    this.authUser$.subscribe(authData => {
      if (authData) {
        this.store.dispatch(refreshLoggedUserByIdRequest({ userId: Number(authData.loggedUserId) }));
      }
    });

    this.skills$.subscribe((skills: Skill[]) => {
      this.dataSource.data = skills;
    });
    this.store.dispatch(refreshSkillsRequest());
  }

  openDialog() {
    const dialogRef = this.dialog.open(CreateSkillComponent, {
      disableClose: true,
    });
    const subscribeSkill = dialogRef.componentInstance.createdSkill.subscribe(skill => {
      this.doCreateSkill(skill);
    });

    dialogRef.afterClosed().subscribe(() => {
      subscribeSkill.unsubscribe();
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  doCreateSkill(skill: Skill) {
    this.store.dispatch(createSkillRequest({ skill }));
  }
}
