import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

import { Skill } from 'src/app/models/skills/skill.model';
import { refreshSkillsRequest } from 'src/app/shared/store/shared.actions';
import { AppState } from 'src/app/state/app-state';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-create-skill',
  templateUrl: './create-skill.component.html',
  styleUrls: ['./create-skill.component.css']
})
export class CreateSkillComponent implements OnInit {

  @Output() createdSkill: EventEmitter<Partial<Skill>> = new EventEmitter<Partial<Skill>>();

  name: string;
  nameFormControl = new FormControl('', [Validators.required, Validators.minLength(2)]);

  existingSkills: string[] = [];

  skills$: Observable<Skill[]> = this.store.pipe(select((state) => state.skills));

  constructor(
    private store: Store<AppState>,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit(): void {
    this.store.dispatch(refreshSkillsRequest());

    this.skills$.subscribe(skills => {
      this. existingSkills = skills.map(skill => skill.name);
    });
  }

  createSkill() {
    const skill: Partial<Skill> = {
      name: this.name
    };
    this.notificator.success('You have successfully created a new Skill');
    return this.createdSkill.emit(skill);
  }
}
