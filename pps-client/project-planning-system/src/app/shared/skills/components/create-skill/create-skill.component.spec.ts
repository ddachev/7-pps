import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EventEmitter } from 'protractor';

import { Skill } from 'src/app/models/skills/skill.model';
import { MaterialModule } from 'src/app/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateSkillComponent } from './create-skill.component';
import { AuthService } from 'src/app/auth/auth.service';
import { APP_BASE_HREF } from '@angular/common';
import { NotificatorService } from 'src/app/core/services/notificator.service';


describe('CreateSkillComponent', () => {
  let component: CreateSkillComponent;
  let fixture: ComponentFixture<CreateSkillComponent>;
  let template;
  let mockStore: MockStore;
  let dispatchSpy;
  let notificationService;

  beforeEach(async(() => {
    const initialState = {
      user: null,
      loggedUser: null,
      auth: null,
      skills: [{
        id: 1,
        name: 'test'
      }]
    };

    notificationService = {
      success() {},
      warn() {},
      error() {}
    };

    TestBed.configureTestingModule({
      imports: [MaterialModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, RouterModule.forRoot([])],
      declarations: [CreateSkillComponent],
      providers: [AuthService, NotificatorService, provideMockStore({ initialState }), { provide: APP_BASE_HREF, useValue: '/' }]
    })
      .overrideProvider(NotificatorService, { useValue: notificationService })
      .compileComponents()
      // tslint:disable-next-line: align
      .then(() => {
        fixture = TestBed.createComponent(CreateSkillComponent);
        mockStore = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        template = fixture.nativeElement;
      });

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {

    it('should dispatch Refresh Skills Request action once', () => {
      // Arrange
      dispatchSpy = spyOn(mockStore, 'dispatch');
      // Act
      component.ngOnInit();
      // Assert
      expect(dispatchSpy).toHaveBeenCalledTimes(1);
    });

    it('should dispatch refreshSkillsRequest action', () => {
      // Arrange
      dispatchSpy = spyOn(mockStore, 'dispatch');
      // Act
      component.ngOnInit();
      // Assert
      expect(dispatchSpy).toHaveBeenCalledWith({ "type": "[Skill] Refresh Skills Request" });
    });

    it('should update existingSkills on init', (done) => {
      // Arrange
      // tslint:disable-next-line: prefer-const
      let skills$: Observable<Skill[]> = of([]);
      // Act
      component.ngOnInit();
      // Assert
      skills$.subscribe(data => {
        expect(component.existingSkills).toEqual(['test']);
        done();
      });
    });

  });

  describe('CreateSkill()', () => {

    it('should call createSkill() on create-btn click', () => {
      // Arrange
      spyOn(component, 'createSkill');
      // tslint:disable-next-line: prefer-const
      let button = fixture.debugElement.nativeElement.querySelector('button');
      // Act
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(component.createSkill).toHaveBeenCalled();
      });
    });

    it('should call createdSkill.emit() on button click', () => {
      // Arrange
      // tslint:disable-next-line: new-parens
      const createdSkill = new EventEmitter;
      spyOn(component, 'createSkill');
      const spy = spyOn(createdSkill, 'emit');
      // Act
      // tslint:disable-next-line: prefer-const
      let button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(spy).toHaveBeenCalled();
      });
    });

    it('should call createdSkill.emit() with correct value', () => {
      // Arrange
      const createdSkill = new EventEmitter();
      spyOn(component, 'createSkill');
      const spy = jest.spyOn(createdSkill, 'emit');
      createdSkill.emit('test');
      // tslint:disable-next-line: prefer-const
      let button = fixture.debugElement.nativeElement.querySelector('button');
      // Act
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(spy).toHaveBeenCalledWith('test');
      });
    });

    it('notificationService.success() once when the response is successful', () => {
      // Arrange
      const createdSkill = new EventEmitter();
      spyOn(component, 'createSkill');
      const spy = jest.spyOn(createdSkill, 'emit');
      const successSpy = jest.spyOn(notificationService, 'success');
      // Act
      createdSkill.emit('test');
      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(successSpy).toHaveBeenCalledTimes(1);
      });
    });

    it('notificationService.success() once when the response is successful', () => {
      // Arrange
      const createdSkill = new EventEmitter();
      spyOn(component, 'createSkill');
      const spy = jest.spyOn(createdSkill, 'emit');
      const successSpy = jest.spyOn(notificationService, 'success');
      // Act
      createdSkill.emit('test');
      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(successSpy).toHaveBeenCalledWith('You have successfully created a new Skill');
      });
    });
  });
});
