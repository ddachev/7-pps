import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsersDataService } from './users-data-service.service';
import { SingleUserComponent } from './components/single-user/single-user.component';
import { MaterialModule } from '../material.module';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { UsersRoutingModule } from './users-routing.module';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { SubordinatesComponent } from './components/subordinates/subordinates/subordinates.component';
import { ManagementSkillPipe } from './pipes/management-skill.pipe';
import { AddSkillPipe } from './pipes/add-skill.pipe';

@NgModule({
  declarations: [
    DashboardComponent,
    SingleUserComponent,
    CreateUserComponent,
    AllUsersComponent,
    SubordinatesComponent,
    ManagementSkillPipe,
    AddSkillPipe
  ],
  providers: [UsersDataService],
  imports: [
    CommonModule,
    MaterialModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [ManagementSkillPipe, AddSkillPipe]
})
export class UsersModule { }
