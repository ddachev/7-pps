import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';

import { AppState } from 'src/app/state/app-state';
import { AllUsersComponent } from '../all-users/all-users.component';
import { Skill } from 'src/app/models/skills/skill.model';
import { User } from 'src/app/models/users/user.model';
import { refreshSkillsRequest } from 'src/app/shared/store/shared.actions';
import { refreshUsersRequest } from '../../store/all-users.actions';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  // tslint:disable-next-line: new-parens
  fb = new FormBuilder;
  currentRole = '';

  form = this.fb.group({
    email: this.fb.control('', [
      Validators.required,
      Validators.minLength(4),
      Validators.email,
    ]),
    password: this.fb.control('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20),
    ]),
    firstName: this.fb.control('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    middleName: this.fb.control('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    lastName: this.fb.control('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    role: this.fb.control('', [
      Validators.required,
    ]),
    managerId: this.fb.control('', [
      Validators.required
    ]),
    position: this.fb.control('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    skills: this.fb.array([
      this.fb.control('', [
        Validators.required,
      ])
    ])
  });

  roles = ['manager', 'employee'];
  managers: User[] = [];
  emailsToCompare = [];

  allUsers$: Observable<User[]> = this.store.pipe(select((state) => state.users));
  skills$: Observable<Skill[]> = this.store.pipe(select((state) => state.skills));

  @Output() createdUser: EventEmitter<User> = new EventEmitter<User>();


  constructor(
    private store: Store<AppState>,
    public dialogRef: MatDialogRef<AllUsersComponent>,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit(): void {
    this.store.dispatch(refreshSkillsRequest());
    this.store.dispatch(refreshUsersRequest());

    this.allUsers$.subscribe(users => {
      this.managers = users.filter(user => user.role === 'manager');
    });

    this.allUsers$.subscribe(users => {
      this.emailsToCompare = users.map(user => user.email);
    });
  }

  get skills() {
    return this.form.get('skills') as FormArray;
  }

  addSkill() {
    this.skills.push(this.fb.control(''));
  }

  createUser() {
    const newUser = this.form.value;
    this.notificator.success('You have successfully created a new User');
    this.createdUser.emit(newUser);
  }

}
