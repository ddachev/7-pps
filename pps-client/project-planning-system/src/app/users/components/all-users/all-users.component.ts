import { Component, OnInit, ViewChild, AfterViewChecked, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import {
  deleteUserRequest,
  refreshUsersRequest,
  createUserRequest,
  refreshLoggedUserByIdRequest
} from '../../store/all-users.actions';
import { AppState } from '../../../state/app-state';
import { User } from '../../../models/users/user.model';
import { CreateUserComponent } from '../create-user/create-user.component';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit, AfterViewInit {

  displayedColumns = ['name', 'role', 'position', 'availability', 'skills', 'tasks'];
  dataSource = new MatTableDataSource<User>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  users$: Observable<User[]> = this.store.pipe(select(state => state.users));
  loggedUser$: Observable<User> = this.store.pipe(select(state => state.loggedUser));

  authUser$: Observable<{
    isLoggedIn: boolean;
    loggedUserId: number;
  }> = this.store.pipe(select((state) => state.auth));

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {

    this.authUser$.subscribe(authData => {
      if (authData) {
        this.store.dispatch(refreshLoggedUserByIdRequest({ userId: Number(authData.loggedUserId) }));
      }
    });

    this.users$.subscribe((users: User[]) => {
      this.dataSource.data = users;
    });

    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'name': return item.firstName;
        case 'position': return item.position.toLocaleLowerCase();
        default: return item[property];
      }
    };

    this.store.dispatch(refreshUsersRequest());
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog() {
    const dialogRef = this.dialog.open(CreateUserComponent, {
      disableClose: true,
    });
    const subscribeUser = dialogRef.componentInstance.createdUser.subscribe(user => {
      this.createUser(user);
    });

    dialogRef.afterClosed().subscribe(() => {
      subscribeUser.unsubscribe();
    });
  }

  createUser(user: User) {
    this.store.dispatch(createUserRequest({ user }));
  }

  deleteUser(userId: number) {
    this.store.dispatch(deleteUserRequest({ userId }));
  }
}
