import { Component, OnInit, OnChanges, SimpleChange, SimpleChanges, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { AppState } from 'src/app/state/app-state';
import { User } from 'src/app/models/users/user.model';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { refreshUserByIdRequest, refreshLoggedUserByIdRequest } from '../../store/all-users.actions';
import { refreshSkillsRequest, addSkillToUserRequest } from 'src/app/shared/store/shared.actions';
import { Skill } from 'src/app/models/skills/skill.model';

@Component({
  selector: 'app-single-user',
  templateUrl: './single-user.component.html',
  styleUrls: ['./single-user.component.css']
})
export class SingleUserComponent implements OnInit, OnDestroy {

  public destroyed = new Subject<any>();
  currentSkill;
  existingSkills;
  showAddSkill = false;
  panelOpenState = true;

  user$: Observable<User> = this.store.pipe(select(state => state.user));

  authUser$: Observable<{
    isLoggedIn: boolean;
    loggedUserId: number;
  }> = this.store.pipe(select((state) => state.auth));

  skills$: Observable<Skill[]> = this.store.pipe(select((state) => state.skills));

  constructor(
    private store: Store<AppState>,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.store.dispatch(refreshSkillsRequest());

    this.authUser$.subscribe(authData => {
      if (authData) {
        this.store.dispatch(refreshLoggedUserByIdRequest({ userId: Number(authData.loggedUserId) }));
      }
    });

    this.store.dispatch(refreshUserByIdRequest({ userId: Number(this.route.snapshot.params.id) }));

    this.user$.subscribe(user => {
      if (user) {
        this.existingSkills = user.skills;
      }
    });

    this.router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this.destroyed)
    ).subscribe(() => {
      this.store.dispatch(refreshUserByIdRequest({ userId: Number(this.route.snapshot.params.id) }));
    });
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

  addSkill() {
    this.store.dispatch(addSkillToUserRequest({userId: Number(this.route.snapshot.params.id), skillId: this.currentSkill}));
    this.showAddSkill = false;
  }
}
