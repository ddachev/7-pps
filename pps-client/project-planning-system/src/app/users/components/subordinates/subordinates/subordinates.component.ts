import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

import { User } from 'src/app/models/users/user.model';
import { AppState } from 'src/app/state/app-state';

@Component({
  selector: 'app-subordinates',
  templateUrl: './subordinates.component.html',
  styleUrls: ['./subordinates.component.css']
})
export class SubordinatesComponent implements OnInit, AfterViewInit {

  panelOpenState1 = false;
  panelOpenState2 = false;
  displayedColumns = ['name', 'availability'];
  dataSource = new MatTableDataSource<User>();

  @ViewChild('sort', { static: false }) sort: MatSort;
  @ViewChild('paginator', { static: false }) paginator: MatPaginator;

  loggedUser$: Observable<User> = this.store.pipe(select(state => state.loggedUser));
  loggedUser: User;

  constructor(
    private readonly store: Store<AppState>
  ) { }

  ngOnInit(): void {

    this.loggedUser$.subscribe(user => {
      if (user) {
        this.dataSource.data = user.subordinates;
      }

      this.loggedUser = user;
    });

    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };
  }


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(searchStr: string) {
    this.dataSource.filter = searchStr.trim().toLowerCase();
  }

}
