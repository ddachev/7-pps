import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AllUsersComponent } from './components/all-users/all-users.component';
import { SingleUserComponent } from './components/single-user/single-user.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  { path: '', component: AllUsersComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'users/dashboard', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'users/:id', component: SingleUserComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class UsersRoutingModule { }
