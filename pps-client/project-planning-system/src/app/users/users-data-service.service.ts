import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from '../models/users/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  getAllUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(`http://localhost:3000/users`);
  }

  createUser(user: User): Observable<User> {
    return this.httpClient.post<User>(`http://localhost:3000/users/register`, user);
  }

  getUserById(userId: number): Observable<User> {
    return this.httpClient.get<User>(`http://localhost:3000/users/${userId}`);
  }

}
