import { PipeTransform, Pipe } from '@angular/core';
import { Skill } from 'src/app/models/skills/skill.model';

@Pipe({
  name: 'filterSkillsForManagers'
})
export class ManagementSkillPipe implements PipeTransform {

  transform(skills: Skill[]): Skill[] {

    return skills.filter(skill => {
      return skill.name === 'Management';
    });
  }
}
