import { PipeTransform, Pipe } from '@angular/core';
import { Skill } from 'src/app/models/skills/skill.model';

@Pipe({
  name: 'filterSkillsForAdding'
})
export class AddSkillPipe implements PipeTransform {

  transform(skills: Skill[], existingSkills: Skill[]): Skill[] {

    const remainigSkills =  skills.reduce((acc, curr) => {
      if (!existingSkills.map(s => s.id).includes(curr.id)) {
        acc.push(curr);
      }
      return acc;
    }, []);

    return remainigSkills;
  }
}
