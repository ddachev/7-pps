import { User } from 'src/app/models/users/user.model';
import { createReducer, on } from '@ngrx/store';
import { refreshUsersDone, refreshUserByIdDone, refreshLoggedUserByIdDone, clearLoggedUserRequest } from './all-users.actions';

export const allUsersReducer = createReducer<User[]>([],
  on(refreshUsersDone, (_, action) => action.users),
);

export const userReducer = createReducer<User>(null,
  on(refreshUserByIdDone, (_, action) => action.user));

export const loggedUserReducer = createReducer<User>(null,
  on(refreshLoggedUserByIdDone, (_, action) => action.user),
  on(clearLoggedUserRequest, (state, action) => {
    return null;
  }));
