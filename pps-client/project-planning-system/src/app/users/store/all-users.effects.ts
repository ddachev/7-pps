import { UsersDataService } from '../users-data-service.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';
import {
  refreshUsersRequest,
  refreshUsersDone,
  createUserRequest,
  refreshUserByIdRequest,
  refreshUserByIdDone,
  refreshLoggedUserByIdRequest,
  refreshLoggedUserByIdDone,
  clearLoggedUserRequest,
  clearLoggedUserDone
} from './all-users.actions';
import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Injectable()
export class AllUsersEffects {
  constructor(
    private readonly usersService: UsersDataService,
    private readonly authService: AuthService,
    private readonly actions$: Actions
  ) { }

  refreshUsers$ = createEffect(() => this.actions$.pipe(
    ofType(refreshUsersRequest),
    switchMap(() => {
      return this.usersService.getAllUsers().pipe(
        map(users => {
          return refreshUsersDone({ users });
        })
      );
    })
  ));

  createUser$ = createEffect(() => this.actions$.pipe(
    ofType(createUserRequest),
    switchMap((action) => {
      return this.usersService.createUser(action.user).pipe(
        map(() => refreshUsersRequest())
      );
    })
  ));

  refreshUserById$ = createEffect(() => this.actions$.pipe(
    ofType(refreshUserByIdRequest),
    switchMap((action) => {
      return this.usersService.getUserById(action.userId).pipe(
        map(user => refreshUserByIdDone({ user }))
      );
    })
  ));

  refreshLoggedUserById$ = createEffect(() => this.actions$.pipe(
    ofType(refreshLoggedUserByIdRequest),
    switchMap((action) => {
      return this.usersService.getUserById(action.userId).pipe(
        map(user => refreshLoggedUserByIdDone({ user }))
      );
    })
  ));

  clearLoggedUser$ = createEffect(() => this.actions$.pipe(
    ofType(clearLoggedUserRequest),
    switchMap(() => {
      return this.authService.logOut().pipe(
        map(() => clearLoggedUserDone())
      );
    })
  ));

}
