import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/models/users/user.model';


export const refreshUsersRequest = createAction('[User] Refresh Users Request');
export const refreshUsersDone = createAction('[User] Refresh Users Done', props<{ users: User[] }>());

export const createUserRequest = createAction('[User] Create User', props<{ user: User }>());
export const deleteUserRequest = createAction('[User] Delete User', props<{ userId: number }>());


export const refreshUserByIdRequest = createAction('[User] Refresh User By Id Request', props<{ userId: number }>());
export const refreshUserByIdDone = createAction('[User] Refresh User By Id Done', props<{ user: User }>());

export const refreshLoggedUserByIdRequest = createAction('[User] Refresh LoggedUser By Id Request', props<{ userId: number }>());
export const refreshLoggedUserByIdDone = createAction('[User] Refresh LoggedUser By Id Done', props<{ user: User }>());

export const clearLoggedUserRequest = createAction('[User] Clear LoggedUser Data');
export const clearLoggedUserDone = createAction('[User] Clear LoggedUser Data Done');
