// tslint:disable-next-line: quotemark
import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { tap } from 'rxjs/operators';

import { AppState } from '../state/app-state';

@Injectable()
export class AuthGuard implements CanActivate {

  loggedIn$ = this.store.pipe(select(state => state.auth.isLoggedIn));

  constructor(
    private readonly router: Router,
    private readonly store: Store<AppState>
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {

    return this.loggedIn$
      .pipe(
        tap(loggedIn => {
          if (!loggedIn) {
            this.router.navigate(['login']);
          }
        })
      );
  }

}
