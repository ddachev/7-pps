import { createAction, props } from '@ngrx/store';

import { User } from 'src/app/models/users/user.model';


export const loginUserRequest = createAction('[User] Login User Request', props<{ user: User }>());
export const autoLoginUserRequest = createAction('[User] Login User Request');
export const userLoginSuccess = createAction('[User] Login User Success', props<any>());
export const userLogoutRequest = createAction('[Auth] Logout User Request');

export const userLoginFailure = createAction('[Auth] Login User Failure', props<any>());
export const userLoginFailureHandle = createAction('[Auth] Login User Failure Handle');
