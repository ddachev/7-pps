import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { loginUserRequest, userLoginSuccess, userLogoutRequest, autoLoginUserRequest, userLoginFailure, userLoginFailureHandle } from './auth.actions';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
  ) { }

  loginUser$ = createEffect(() => this.actions$.pipe(
    ofType(loginUserRequest),
    switchMap((action) => {
      return this.authService.logIn(action.user)
        .pipe(
          map((user) => {
            return userLoginSuccess(user);
          }),
          catchError(error => {
            return of(userLoginFailure({ error }));
          })
        );
    })
  ));

  autoLoginUser$ = createEffect(() => this.actions$.pipe(
    ofType(autoLoginUserRequest),
    switchMap(() => {
      return this.authService.autoLogIn()
        .pipe(
          map((user) => {
            return userLoginSuccess(user);
          })
        );
    })
  ));

  logoutUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(userLogoutRequest),
      switchMap(() => {
        return this.authService.logOut()
          .pipe(
            map(() => {
              return userLogoutRequest();
            })
          );
      })
    );
  });

  loginFailure$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(userLoginFailure),
      switchMap(() => {
        return this.authService.loginFailure()
          .pipe(
            map(() => {
              return userLoginFailureHandle();
            })
          );
      })
    );
  });

}
