// tslint:disable-next-line: quotemark
import { createReducer, on, State } from "@ngrx/store";

import { loginUserRequest, userLoginSuccess, userLogoutRequest, autoLoginUserRequest } from './auth.actions';


export const authReducer = createReducer<any>({
    isLoggedIn: false,
    loggedUserId: null
  },
  on(loginUserRequest, (state, action) => {
    return {
      ...state
    };
  }),
  on(autoLoginUserRequest, (state, action) => {
    return {
      ...state
    };
  }),
  on(userLoginSuccess, (state, action) => {
    return {
      ...state,
      isLoggedIn: true,
      loggedUserId: action.id,
    };
  }),
  on(userLogoutRequest, (state, action) => {
    return {
      ...state,
      isLoggedIn: false,
      loggedUserId: null
    };
  }),
);
