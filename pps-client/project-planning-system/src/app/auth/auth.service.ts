import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

import { StorageService } from '../core/services/storage.service';
import { User } from '../models/users/user.model';
import { NotificatorService } from '../core/services/notificator.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly helper = new JwtHelperService();
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  logIn(user: User) {
    return this.http.post<{ accessToken: string }>(`http://localhost:3000/auth/session`, user)
      .pipe(
        map(({ accessToken }) => {
          this.storage.save('token', accessToken);
          this.notificator.success('You have successfully logged in.');
          this.router.navigate(['users/dashboard']);
          return this.helper.decodeToken(accessToken);
        })
      );
  }

  logOut() {
    this.storage.save('token', '');
    this.notificator.success('You have successfully logged out.');
    this.router.navigate(['home']);
    return new Observable();
  }

  autoLogIn() {
    const token = this.storage.read('token');
    if (!token) {
      return;
    }
    const user = this.helper.decodeToken(token);
    return of(user);
  }

  loginFailure() {
    this.notificator.error('Wrong username/password. Please, try again.');
    return new Observable();
  }

}
