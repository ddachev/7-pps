# 04. Project planning system - Team 7

###Project description
-
***
###How to run it
- **Setup Database**

- Install MySQL and MySQL Workbench;
- Setup a new connection;
- Create new Schema = 'pps1';
- **Setup Environment**
- Download all files and hit npm install in the root folder of the project;
- Create .env file, looking like this:
***example:***
  ```javascript
    DB_TYPE=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_USERNAME=root
    DB_PASSWORD=123456
    DB_DATABASE_NAME=pps1
  ```
- create ormconfig.json file, looking like this:
  ***example:***
  ```javascript
    {
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "123456",
    "database": "pps1",
    "entities": [
        "src/database/entities/**/*.ts"
    ],
    "migrations": [
        "src/database/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/database/entities",
        "migrationsDir": "src/database/migration"
        }
    }
  ```
- npm run seed; This script will create basic roles and one admin;
- npm run start:dev; This script will start our server in watch-mode;

###How to use the PPS
- admin username: admin@abv.bg
- admin password: Aaa123
- **Endpoints**

- You can find a Postman Collection in the root folder of the project;
- In order to protect your privacy, please change the password in the .end and ormconfig.json files

- **Front-end**
- In order to run the client, just hit npm install in the client folder of the project
- ng serve
  ***