import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from '@nestjs/config';
import { Connection } from 'typeorm';
import { ProjectsModule } from './projects/projects.module';
import { SkillsModule } from './skills/skills.module';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './core/core.module';
import { PPSExceptionsFilter } from './common/filters/pps-exceptions.filter';
import { APP_FILTER } from '@nestjs/core';
import { ScheduleModule } from '@nestjs/schedule';


@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().default(3000),
        DB_TYPE: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USERNAME: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE_NAME: Joi.string().required(),
      }),
    }),
    AuthModule,
    CoreModule,
    UsersModule,
    DatabaseModule,
    ProjectsModule,
    SkillsModule,
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: APP_FILTER, useClass: PPSExceptionsFilter}
  ],
})
export class AppModule {
  constructor(private connection: Connection) { }
}
