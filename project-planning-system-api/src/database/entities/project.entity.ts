/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { ProjectStatus } from '../../common/enums/projectStatus.enum';
import { User } from './user.entity';
import { Resource } from './resources.entity';
import { Task } from './tasks.entity';


@Entity('projects')
export class Project {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'nvarchar', nullable: false })
    title: string;

    @Column({ type: 'text', nullable: false })
    description: string;

    @Column({ default: ProjectStatus.inProgress })
    status: ProjectStatus;

    @Column({ type: 'date', nullable: false })
    startDate: Date;

    @Column({ type: 'date', nullable: false })
    endDate: Date;

    @Column({ type: 'date', nullable: false })
    completeDate: Date;

    @ManyToOne(type => User, manager => manager.managedProjects)
    manager: Promise<User>;

    @OneToMany(type => Resource, resource => resource.project, { eager: true })
    resources: Resource[];

    @OneToMany(type => Task, task => task.project)
    tasks: Promise<Task[]>;
}