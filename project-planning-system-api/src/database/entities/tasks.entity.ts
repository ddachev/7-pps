import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Skill } from "./skill.entity";
import { User } from "./user.entity";
import { Project } from "./project.entity";


@Entity('tasks')
export class Task {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false })
    hours: number;

    @Column({ type: 'date', nullable: false })
    startDate: Date;

    @Column({ type: 'date', nullable: false })
    deleteDate: Date;

    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;

    @ManyToOne(type => Skill, {eager: true})
    skill: Skill;
    // skill => skill.tasks

    @ManyToOne(type => User, user => user.tasks)
    user: Promise<User>;

    @ManyToOne(type => Project, project => project.tasks)
    project: Promise<Project>;
}