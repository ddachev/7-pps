import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, Unique } from "typeorm";
import { Resource } from "./resources.entity";
import { Task } from "./tasks.entity";


@Entity('skills')
@Unique(['name'])
export class Skill {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false })
    name: string;

    // @OneToMany(type => Resource, resource => resource.skill)
    // resources: Resource[];

    // @OneToMany(type => Task, task => task.skill)
    // tasks: Task[];
}