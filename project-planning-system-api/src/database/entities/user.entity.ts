import { Entity, Column, PrimaryGeneratedColumn, Unique, OneToMany, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Role } from './roles.entity';
import { UserRoles } from '../../common/enums/roles.enum';
import { UserPositions } from '../../common/enums/positions.enum';
import { Skill } from './skill.entity';
import { Project } from './project.entity';
import { Task } from './tasks.entity';


@Entity('users')
@Unique(['email'])
export class User {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'nvarchar', nullable: false })
    password: string;

    @Column({ type: 'nvarchar', nullable: false })
    firstName: string;

    @Column({ type: 'nvarchar', nullable: false })
    middleName: string;

    @Column({ type: 'nvarchar', nullable: false })
    lastName: string;

    @Column({ type: 'nvarchar', nullable: false })
    position: string;

    @Column({ type: 'nvarchar', nullable: false })
    email: string;

    @Column({ type: 'int', default: 8 })
    availability: number;

    @Column({ type: 'boolean', default: true })
    availabilityStatus: boolean;

    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;

    @ManyToOne(type => Role, { eager: true })
    role: Role;

    // @Column({ nullable: true })
    // avatarUrl: string;

    @ManyToOne(type => User, user => user.subordinates, {nullable: true})
    manager: Promise<User>;

    @OneToMany(type => User, user => user.manager, {nullable: true})
    subordinates: Promise<User[]>;
    
    @ManyToMany(type => Skill, { eager: true })
    @JoinTable()
    skills: Skill[];
    
    @OneToMany(type => Task, task => task.user)
    tasks: Promise<Task[]>;
    
    @OneToMany(type => Project, project => project.manager, {nullable: true})
    managedProjects: Promise<Project[]>;
    
}