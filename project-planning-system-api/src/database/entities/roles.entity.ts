import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";


@Entity('roles')
export class Role {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false })
    name: string
}