import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne } from "typeorm";
import { Skill } from "./skill.entity";
import { Project } from "./project.entity";


@Entity('resources')
export class Resource {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false })
    hours: number;

    @ManyToOne(type => Skill, {eager: true})
    skill: Skill; 
    // skill => skill.resources

    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;

    @ManyToOne(type => Project, project => project.resources)
    project: Promise<Project>; 
}