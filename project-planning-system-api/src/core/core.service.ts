import { Injectable } from '@nestjs/common';
import { User } from '../database/entities/user.entity';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { ReturnUserWithoutRelationsDTO } from '../users/models/return-user-without-relations.dto';
import { ReturnProjectDTO } from '../projects/models/return-project.dto';
import { Project } from '../database/entities/project.entity';
import { Task } from 'src/database/entities/tasks.entity';

@Injectable()
export class CoreService {

    toUserReturnDTO(user: User): ReturnUserDTO {
        return {
            id: user.id,
            firstName: user.firstName,
            middleName: user.middleName,
            lastName: user.lastName,
            email: user.email,
            role: user.role.name,
            position: user.position,
            availability: user.availability,
            skills: user.skills,
            tasks: ((user as any).__tasks__),
            manager: ((user as any).__manager__ && this.toUserWithoutRelationsReturnDTO((user as any).__manager__)),
            subordinates: ((user as any).__subordinates__ && (user as any).__subordinates__.map(x => this.toUserWithoutRelationsReturnDTO(x))),
            managedProjects: ((user as any).__managedProjects__ && (user as any).__managedProjects__.map(x => this.toProjectReturnDTO(x))),
        }
    }


    toUserWithoutRelationsReturnDTO(user: User): ReturnUserWithoutRelationsDTO {
        return {
            id: user.id,
            email: user.email,
            firstName: user.firstName,
            middleName: user.middleName,
            lastName: user.lastName,
            // role: user.role.name,
            skills: user.skills,
            position: user.position,
            availability: user.availability,
        }
    }


    toProjectReturnDTO(project: Project): ReturnProjectDTO {
        return {
            id: project.id,
            title: project.title,
            description: project.description,
            startDate: project.startDate,
            endDate: project.endDate,
            completeDate: project.completeDate,
            status: project.status,
            manager: (project as any).__manager__ && this.toUserWithoutRelationsReturnDTO((project as any).__manager__),
            resources: project.resources,
            // tasks: ((project as any).__tasks__&& (project as any).__tasks__.map(x => this.toTaskReturnDto(x))),
            tasks: ((project as any).__tasks__ && (project as any).__tasks__.map(x => this.toTaskReturnDto(x)))

        }
    }

    toTaskReturnDto(task: Task) {
        return {
            id: task.id,
            hours: task.hours,
            startDate: task.startDate,
            isDeleted: task.isDeleted,
            skill: task.skill,
            user: (task as any).__user__ && this.toUserWithoutRelationsReturnDTO((task as any).__user__),
            project: (task as any).__project__ && this.toProjectReturnDTO((task as any).__project__),
        }
    }
}
