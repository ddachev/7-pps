import { Module } from '@nestjs/common';
import { CoreService } from './core.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/roles.entity';
import { Skill } from '../database/entities/skill.entity';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Skill]),
    AuthModule,
  ],
  providers: [CoreService],
  exports: [AuthModule],
})
export class CoreModule {}
