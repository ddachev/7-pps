import { Controller, Post, Body, Get, Param, ParseIntPipe, Delete, Put, UseGuards } from '@nestjs/common';
import { SkillsService } from './skills.service';
import { SkillCreateDTO } from './models/create-skill.dto';
import { Skill } from '../database/entities/skill.entity';
import { SkillUpdateDTO } from './models/update-skill.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('skills')
@UseGuards(AuthGuard('jwt'))
export class SkillsController {

    constructor(
        private readonly skillsService: SkillsService,
    ) { }


    @Post()
    async createSkill(
        @Body() skillCreateDTO: SkillCreateDTO): Promise<Skill> {
            return await this.skillsService.createSkill(skillCreateDTO);
        }

    
    @Get()
    async getAllSkills(): Promise<Skill[]> {
        return await this.skillsService.getAllSkills();
    }


    @Get('/:skillId')
    async findSkillById(
        @Param('skillId', ParseIntPipe) skillId: number): Promise<Skill> {
            return await this.skillsService.findSkillById(skillId);
        }


    @Put('/:skillId')
    async updateSkill(
        @Param('skillId', ParseIntPipe) skillId: number,
        @Body() skillUpdateDTO: SkillUpdateDTO): Promise<Skill> {
            return await this.skillsService.updateSkill(skillId, skillUpdateDTO)
        }   

        
    @Delete('/:skillId')
    async deleteSkill(
        @Param('skillId', ParseIntPipe) skillId: number): Promise<{msg: string}> {
            return await this.skillsService.deleteSkill(skillId);
        }
}
