import { Injectable, ConflictException, InternalServerErrorException } from '@nestjs/common';
import { Skill } from '../database/entities/skill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SkillCreateDTO } from './models/create-skill.dto';
import { SkillUpdateDTO } from './models/update-skill.dto';

@Injectable()
export class SkillsService {


    constructor(
        @InjectRepository(Skill)
        private readonly skillsRepo: Repository<Skill>,
    ) { }


    async createSkill(skillCreateDTO: SkillCreateDTO): Promise<Skill> {
        const newSkill = this.skillsRepo.create(skillCreateDTO);

        let savedSkill;

        try {
            savedSkill = await this.skillsRepo.save(newSkill);
        } catch (error) {
            if (error.code === 'ER_DUP_ENTRY') {
                throw new ConflictException('This skill already exists!')
            } else {
                throw new InternalServerErrorException();
            }
        }

        return savedSkill;
    }


    async getAllSkills(): Promise<Skill[]> {
        const foundSkills = await this.skillsRepo.find({
            order: { id: "DESC" }
        });
                
        return foundSkills;
    }


    async findSkillById(id: number): Promise<Skill> {

        const foundSkill = await this.skillsRepo.findOne({
            where: {
                id,
            },
        });

        return foundSkill;
    }


    async updateSkill(id: number, skillUpdateDTO: SkillUpdateDTO): Promise<Skill> {
        const foundSkill = await this.findSkillById(id);

        // const isAdmin = user.roles.map(r => r.name).includes('admin');

        // if (!isAdmin) {
        //     throw new ForumError(`You are not authorized to update this post!`, 404);
        // }

        foundSkill.name = skillUpdateDTO.name;
        const savedSkill = await this.skillsRepo.save(foundSkill);
        
        return savedSkill;
    }


    async deleteSkill(id: number): Promise<{msg: string}> {
        const foundSkill = await this.findSkillById(id);
        await this.skillsRepo.delete(foundSkill);

        return {
            msg: `You have successfully deleted ${foundSkill.name} skill`
        }
    }


}
