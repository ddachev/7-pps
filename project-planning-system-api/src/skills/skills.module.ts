import { Module } from '@nestjs/common';
import { SkillsController } from './skills.controller';
import { SkillsService } from './skills.service';
import { Skill } from '../database/entities/skill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([Skill]),
    PassportModule
  ],
  controllers: [SkillsController],
  providers: [SkillsService]
})
export class SkillsModule {}
