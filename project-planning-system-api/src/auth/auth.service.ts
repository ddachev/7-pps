import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import { Role } from '../database/entities/roles.entity';
import { JwtService } from '@nestjs/jwt';
import { SignInUserDTO } from './models/signIn-user.dto';
import * as bcrypt from 'bcrypt'

@Injectable()
export class AuthService {


    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepo: Repository<Role>,
        private readonly jwtservice: JwtService,
    ) { }

    async signIn(signInUserDTO: SignInUserDTO): Promise<{ accessToken: string }> {

        const { email, password } = signInUserDTO;
        const foundUser = await this.userRepo.findOne({
            where: {
                email: email
            },
        });

        if (foundUser && await this.validateUserPassword(password, foundUser.password)) {
            const payload = { email, id: foundUser.id };
            const accessToken = this.jwtservice.sign(payload);

            return { accessToken };
        } 
        // else {
        //     throw new ForumError('Invalid username and/or password!', 404);
        // }
    }


    private async validateUserPassword(password: string, hashedPassword: string): Promise<boolean> {
        return bcrypt.compare(password, hashedPassword);
    }
}
