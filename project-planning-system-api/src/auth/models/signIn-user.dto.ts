export class SignInUserDTO {
    public email: string;
    public password: string;
}