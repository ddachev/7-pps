import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../database/entities/user.entity';
import { PPSError } from 'src/common/exceptions/pps-error';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: 'temporarySecret',
        });
    }

    
    async validate(payload: any): Promise<User> {
        const { email } = payload;
        const user = await this.userRepo.findOne({ email });

        if(!user) {
            throw new PPSError('Such username does not exist', 400)
        }

        return user;
    }
}