import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInUserDTO } from './models/signIn-user.dto';

@Controller('auth')
export class AuthController {

    constructor(
        private readonly authService: AuthService,
    ) { }


    @Post('/session')
    public async signIn(@Body() signInUserDTO: SignInUserDTO): Promise<{ accessToken: string }> {
        return this.authService.signIn(signInUserDTO);
    }

}
