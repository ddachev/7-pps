import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Role } from '../../database/entities/roles.entity';


@Injectable()
export class AdminGuard implements CanActivate {
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        return this.validateRequest(request);
    }

    private validateRequest(request: any): boolean {
        const roleArray: Role[] = request.user.roles;
        const userRoles: string[] = roleArray.map(role => role.name)

        if (userRoles.includes('admin')) {
            return true;
        }

        return false;
    }
}