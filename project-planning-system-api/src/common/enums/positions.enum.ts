export enum UserPositions {
    junior = 'junior',
    midlevel = 'mid-level',
    senior = 'senior',
    administrator = 'administrator',
}