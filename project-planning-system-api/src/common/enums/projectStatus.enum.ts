export enum ProjectStatus {
    inProgress = 'in-progress',
    stopped = 'stopped',
    completed = 'completed'
}