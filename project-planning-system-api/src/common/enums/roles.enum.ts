export enum UserRoles {
    admin = 'admin',
    manager = 'manager',
    employee  = 'employee'
}