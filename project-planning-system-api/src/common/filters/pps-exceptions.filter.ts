import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { PPSError } from '../exceptions/pps-error';


@Catch(PPSError)
export class PPSExceptionsFilter<T = PPSError> implements ExceptionFilter {
  catch(exception: PPSError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest<Request>();
    // const status = exception.getStatus();

    response.status(exception.status).json({
      statusCode: exception.status,
      timestamp: new Date().toISOString(),
      path: request.url,
      message: exception.message
    });
  }
}
