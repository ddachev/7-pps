import { Controller, Body, Post, Get, Param, ParseIntPipe, UseGuards, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDTO } from './models/create-user.dto';
import { User } from '../database/entities/user.entity';
import { ReturnUserDTO } from './models/return-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/common/guards/admin.guard';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class UsersController {

    constructor(
        private readonly usersService: UsersService,
    ) { }


    @Post('/register')
    public async registerUser(@Body() сreateUserDTO: CreateUserDTO): Promise<ReturnUserDTO> {
        return this.usersService.registerUser(сreateUserDTO);
    }

    @Get()
    async getAllUsers(): Promise<ReturnUserDTO[]> {
        return await this.usersService.getAllUsers();
    }


    @Get('/:userId')
    async getUserById(@Param('userId', ParseIntPipe) userId: number): Promise<ReturnUserDTO> {
        return await this.usersService.getUserById(userId);
    }

    @Put('/:userId/skills/:skillId')
    async addSkill(
        @Param('userId', ParseIntPipe) userId: number,
        @Param('skillId', ParseIntPipe) skillId: number): Promise<User> {
        return await this.usersService.addSkill(userId, skillId);
    }


}
