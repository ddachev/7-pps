import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from '../database/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '../database/entities/roles.entity';
import { Skill } from 'src/database/entities/skill.entity';
import { CoreModule } from 'src/core/core.module';
import { AuthModule } from 'src/auth/auth.module';
import { CoreService } from 'src/core/core.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Skill]),
    CoreModule,
    AuthModule,
    PassportModule
  ],
  controllers: [UsersController],
  providers: [UsersService, CoreService]
})
export class UsersModule {


}
