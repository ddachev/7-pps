/* eslint-disable prefer-const */
import { Injectable, ConflictException, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository, In } from 'typeorm';
import { Role } from '../database/entities/roles.entity';
import { CreateUserDTO } from './models/create-user.dto';
import { Skill } from '../database/entities/skill.entity';
import * as bcrypt from 'bcrypt'
import { ReturnUserDTO } from './models/return-user.dto';
import { CoreService } from '../core/core.service';
import { PPSError } from 'src/common/exceptions/pps-error';
import { UserRoles } from 'src/common/enums/roles.enum';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepo: Repository<Role>,
        @InjectRepository(Skill) private readonly skillsRepo: Repository<Skill>,
        private readonly coreService: CoreService,
    ) { }


    async registerUser(сreateUserDTO: CreateUserDTO): Promise<ReturnUserDTO> {
        if (сreateUserDTO.role === UserRoles.manager) {
            return this.registerManager(сreateUserDTO);
        }

        return this.registerEmployee(сreateUserDTO);
    }


    async registerManager(сreateUserDTO: CreateUserDTO): Promise<ReturnUserDTO> {
        const userRole: Role = await this.rolesRepo.findOne({
            where: {
                name: сreateUserDTO.role
            }
        });

        // let usersManager: User = await this.userRepo.findOne({
        //     where: {
        //         name: сreateUserDTO.manager
        //     }
        // });

        const userSkills: Skill = await this.skillsRepo.findOne({
            where: {
                name: сreateUserDTO.skills
            }
        });

        // trifon_dachev@abv.bg

        const newUser = this.userRepo.create();
        newUser.password = await this.hashPassword(сreateUserDTO.password);
        newUser.firstName = сreateUserDTO.firstName;
        newUser.middleName = сreateUserDTO.middleName;
        newUser.lastName = сreateUserDTO.lastName;
        newUser.position = сreateUserDTO.position;
        newUser.email = сreateUserDTO.email;
        newUser.role = userRole;
        newUser.skills = [userSkills];
        newUser.subordinates = Promise.resolve([]);
        newUser.tasks = Promise.resolve([]);
        newUser.managedProjects = Promise.resolve([]);

        let registeredUser;

        try {
            registeredUser = await this.userRepo.save(newUser)
        } catch (error) {
            if (error.code === 'ER_DUP_ENTRY') {
                throw new ConflictException('Email already exists!')
            } else {
                throw new InternalServerErrorException();
            }
        }

        return this.coreService.toUserReturnDTO(registeredUser);
    }

    async registerEmployee(сreateUserDTO: CreateUserDTO): Promise<ReturnUserDTO> {
        const userRole: Role = await this.rolesRepo.findOne({
            where: {
                name: сreateUserDTO.role
            }
        });

        const userManager: User = await this.userRepo.findOne({
            where: {
                id: сreateUserDTO.managerId
            }
        });

        const skillsArray: Skill[] = await this.skillsRepo.find({
            where: {
                name: In(сreateUserDTO.skills)
            }
        });

        const newUser = this.userRepo.create();
        newUser.password = await this.hashPassword(сreateUserDTO.password);
        newUser.firstName = сreateUserDTO.firstName;
        newUser.middleName = сreateUserDTO.middleName;
        newUser.lastName = сreateUserDTO.lastName;
        newUser.position = сreateUserDTO.position;
        newUser.email = сreateUserDTO.email;
        newUser.role = userRole;
        newUser.skills = skillsArray;
        newUser.subordinates = Promise.resolve([]);
        newUser.tasks = Promise.resolve([]);
        newUser.managedProjects = Promise.resolve([]);
        newUser.manager = Promise.resolve(userManager);

        let registeredUser;

        try {
            registeredUser = await this.userRepo.save(newUser);
        } catch (error) {
            if (error.code === 'ER_DUP_ENTRY') {
                throw new ConflictException('Email already exists!')
            } else {
                throw new InternalServerErrorException();
            }
        }

        return this.coreService.toUserReturnDTO(registeredUser);
    }


    private async hashPassword(password: string): Promise<string> {
        const salt = await bcrypt.genSalt();
        return bcrypt.hash(password, salt)
    }


    async getAllUsers(): Promise<ReturnUserDTO[]> {
        const users: User[] = await this.userRepo.find({
            where: {
                isDeleted: false,
            },
            relations: ['manager', 'subordinates', 'skills', 'tasks', 'managedProjects'],
            order: { id: "DESC"}
        });

        return users.map(user => this.coreService.toUserReturnDTO(user));
    }


    async getUserById(userId: number): Promise<ReturnUserDTO> {
        const foundUser: User = await this.userRepo.findOne({
            where: {
                id: userId,
            },
            relations: ['manager', 'subordinates', 'skills', 'managedProjects'],
            join: {
                alias: "user",
                leftJoinAndSelect: {
                    "tasks": "user.tasks",
                    "project": "tasks.project",
                    "skill": "tasks.skill",
                }
            }
            // relations: ['manager', 'subordinates', 'skills', 'tasks', 'managedProjects'],
        })

        if (!foundUser) {
            throw new PPSError(`There is no user with id ${userId}!`, 404);
        }

        return this.coreService.toUserReturnDTO(foundUser);
    }


    async addSkill(userId: number, skillId: number): Promise<User> {

        const foundUser = await this.userRepo.findOne({
            where: {
                id: userId
            }
        })
        const foundSkill = await this.skillsRepo.findOne({
            where: {
                id: skillId
            }
        })

        foundUser.skills.push(foundSkill);
        const savedUser = await this.userRepo.save(foundUser);

        return savedUser;
    }
}
