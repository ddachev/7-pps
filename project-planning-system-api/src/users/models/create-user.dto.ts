import { Role } from "../../database/entities/roles.entity";
import { User } from "../../database/entities/user.entity";
import { Skill } from "../../database/entities/skill.entity";

export class CreateUserDTO {
    email: string;
    password: string;
    firstName: string;
    middleName: string;
    lastName: string;
    role: string;
    managerId?: number;
    skills?: string[];
    subordinates?: User[];
    position: string;
}