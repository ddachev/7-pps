import { ReturnUserWithoutRelationsDTO } from "./return-user-without-relations.dto";
import { User } from "src/database/entities/user.entity";
import { ReturnProjectDTO } from "../../projects/models/return-project.dto";
import { Task } from "../../database/entities/tasks.entity";
import { Skill } from "../../database/entities/skill.entity";

export class ReturnUserDTO {
    id: number;
    email: string;
    firstName: string;
    middleName: string;
    lastName: string;
    role?: string;
    manager?: ReturnUserWithoutRelationsDTO;
    skills?: Skill[];
    subordinates?: ReturnUserWithoutRelationsDTO[];
    position: string;
    managedProjects: ReturnProjectDTO[];
    availability: number;
    tasks: Task[];
}