import { Skill } from "../../database/entities/skill.entity";

export class ReturnUserWithoutRelationsDTO {
    id: number;
    email: string;
    firstName: string;
    middleName: string;
    lastName: string;
    role?: string;
    skills?: Skill[];
    position: string;
    availability: number;
}