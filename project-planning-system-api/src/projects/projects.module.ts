import { Module } from '@nestjs/common';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/roles.entity';
import { Skill } from '../database/entities/skill.entity';
import { Resource } from '../database/entities/resources.entity';
import { Task } from '../database/entities/tasks.entity';
import { Project } from '../database/entities/project.entity';
import { AuthModule } from '../auth/auth.module';
import { CoreService } from '../core/core.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Skill, Resource, Task, Project]),
    AuthModule,
    PassportModule,
  ],
  controllers: [ProjectsController],
  providers: [ProjectsService, CoreService]
})
export class ProjectsModule {}
