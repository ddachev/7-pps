import { Controller, Body, Post, Get, Delete, Param, ParseIntPipe, UseGuards, Put } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { CreateProjectDTO } from './models/create-project.dto';
import { ReturnProjectDTO } from './models/return-project.dto';
import { Project } from 'src/database/entities/project.entity';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/common/decorators/getUser.decorator';
import { User } from 'src/database/entities/user.entity';
import { Task } from 'src/database/entities/tasks.entity';
import { CreateResourceDTO } from './models/add-resource.dto';
import { CreateTaskDTO } from './models/add-task.dto';
import { Resource } from 'src/database/entities/resources.entity';


@Controller('projects')
@UseGuards(AuthGuard('jwt'))
export class ProjectsController {

    constructor(
        private readonly projectService: ProjectsService,
    ) { }


    @Post()
    async createProject(
        @Body() createProjectDTO: CreateProjectDTO,
        @GetUser() user: User
    ): Promise<ReturnProjectDTO> {
        return this.projectService.createProject(createProjectDTO, user);
    }


    @Post('/:projectId/tasks')
    async addTasks(
        @Body() createTaskDTO: CreateTaskDTO,
        @Param('projectId') projectId: number): Promise<Project> {
        return this.projectService.addTasks(createTaskDTO, projectId);
    }


    @Post('/:projectId/resource')
    async addResource(
        @Body() body: any,
        @Param('projectId', ParseIntPipe) projectId: number): Promise<Project> {
        console.log(body.skillId)
        // return this.projectService.addResource(projectId, body.resource, body);
        return this.projectService.addResource(projectId, { skillId: body.skillId, hours: body.hours }, { tasks: body.tasks })
    }


    @Get()
    async getAllProjects(): Promise<ReturnProjectDTO[]> {
        return this.projectService.getAllProjects();
    }


    @Get('/my')
    async getUsersProjects(
        @GetUser() user: User): Promise<ReturnProjectDTO[]> {
        return await this.projectService.getUsersProjects(user);
    }


    @Get('/:projectId')
    async getProjectById(@Param('projectId', ParseIntPipe) projectId: number): Promise<ReturnProjectDTO> {
        return await this.projectService.getProjectByIdPublic(projectId);
    }

    @Put('/:projectId')
    async stopProjectById(@Param('projectId', ParseIntPipe) projectId: number): Promise<Project> {
        console.log('vikni Controller', projectId)
        return await this.projectService.stopProject(projectId);
    }

    @Put('/:projectId/resources/:resourceId')
    async updateResourceTime(
        @Body() body: any,
        @Param('resourceId', ParseIntPipe) resourceId: number): Promise<Resource> {

        return await this.projectService.updateResourceTime(resourceId, body.hours);
    }

    // @Delete('/:projectId')
    // async deleteProject(
    //     @Param('projectId', ParseIntPipe) projectId: number): Promise<{msg: string}> {
    //         return await this.projectService.deleteProject(projectId);
    //     }


    @Delete('/:projectId/tasks/:taskId')
    async removeTask(@Param('taskId', ParseIntPipe) taskId: number): Promise<Task> {
        return await this.projectService.removeTask(taskId);
    }


    @Delete('/:projectId/resource/:resourceId')
    async deleteResource(
        @Param('projectId', ParseIntPipe) projectId: number,
        @Param('resourceId', ParseIntPipe) resourceId: number): Promise<Resource> {
        return await this.projectService.deleteResource(projectId, resourceId)
    }
}
