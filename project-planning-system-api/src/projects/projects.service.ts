import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository, In } from 'typeorm';
import { Role } from '../database/entities/roles.entity';
import { Skill } from '../database/entities/skill.entity';
import { Project } from '../database/entities/project.entity';
import { Resource } from '../database/entities/resources.entity';
import { Task } from '../database/entities/tasks.entity';
import { CreateProjectDTO } from './models/create-project.dto';
import { CoreService } from '../core/core.service';
import { ReturnProjectDTO } from './models/return-project.dto';
import { PPSError } from '../common/exceptions/pps-error';
import { CreateTaskDTO } from './models/add-task.dto';
import { CreateResourceDTO } from './models/add-resource.dto';
import { Cron } from '@nestjs/schedule';
import { ProjectStatus } from 'src/common/enums/projectStatus.enum';

@Injectable()
export class ProjectsService {


    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepo: Repository<Role>,
        @InjectRepository(Skill) private readonly skillsRepo: Repository<Skill>,
        @InjectRepository(Project) private readonly projectRepo: Repository<Project>,
        @InjectRepository(Task) private readonly taskRepo: Repository<Task>,
        @InjectRepository(Resource) private readonly resourceRepo: Repository<Resource>,
        private readonly coreService: CoreService,
    ) { }


    async createProject(createProjectDTO: CreateProjectDTO, user: User): Promise<ReturnProjectDTO> {

        const newProject = this.projectRepo.create();
        newProject.title = createProjectDTO.title;
        newProject.description = createProjectDTO.description;
        newProject.startDate = new Date();
        newProject.completeDate = new Date();
        newProject.endDate = new Date(createProjectDTO.endDate);
        newProject.tasks = Promise.resolve([]);

        const projectManager = await this.userRepo.findOne({
            where: {
                id: user.id,
                availabilityStatus: true,
            }
        });

        if (!projectManager) {
            throw new PPSError(`This manager is not available!`, 404);
        }

        const foundManagementSkill = await this.skillsRepo.findOne({
            where: {
                name: 'Management'
            }
        });
        const managersTask = this.taskRepo.create();
        managersTask.hours = 2;
        managersTask.skill = foundManagementSkill;
        managersTask.startDate = new Date();
        managersTask.deleteDate = new Date();
        // decorators and authGuard!!!!!!!!
        newProject.manager = Promise.resolve(projectManager);


        const savedManagerTask = await this.taskRepo.save(managersTask);
        // managersTask.project = Promise.resolve(savedProject);

        (await newProject.tasks).push(savedManagerTask);
        const savedProject = await this.projectRepo.save(newProject);

        projectManager.availability -= managersTask.hours;
        if (projectManager.availability <= 0) {
            projectManager.availabilityStatus = false;
        }
        projectManager.tasks = Promise.resolve([managersTask]);
        await this.userRepo.save(projectManager);


        const resourcesArray: any = createProjectDTO.resources.map(async (resource: any) => {
            const newResource: Partial<Resource> = this.resourceRepo.create();
            newResource.hours = resource.hours;
            const currSkill = await this.skillsRepo.findOne({
                where: {
                    id: resource.skillId
                }
            });
            newResource.skill = currSkill;
            newResource.project = Promise.resolve(savedProject);
            const savedResource = await this.resourceRepo.save(newResource);
        });


        const tasksArray = [];

        for (let i = 0; i < createProjectDTO.tasks.length; i++) {
            // const tasksArray: any = createProjectDTO.tasks.map(async (task: any) => {
            const newTask: Partial<Task> = this.taskRepo.create();

            const currSkill = await this.skillsRepo.findOne({
                where: {
                    id: createProjectDTO.tasks[i].skillId
                }
            });

            const foundUser = await this.userRepo.findOne({
                where: {
                    id: createProjectDTO.tasks[i].userId,
                    // availabilityStatus: true
                }
            });

            if (!foundUser) {
                throw new PPSError(`There is no user with id ${createProjectDTO.tasks[i].userId}!`, 404);
            }

            newTask.hours = +createProjectDTO.tasks[i].hours;
            newTask.skill = currSkill;
            newTask.startDate = new Date();
            newTask.deleteDate = new Date();
            newTask.project = Promise.resolve(savedProject);
            newTask.user = Promise.resolve(foundUser);

            if (foundUser.availability - createProjectDTO.tasks[i].hours < 0) {
                throw new PPSError(`This employee is not a slave! His availability is less than ${createProjectDTO.tasks[i].hours}`, 404);
            }
            foundUser.availability -= createProjectDTO.tasks[i].hours;

            if (foundUser.availability === 0) {
                foundUser.availabilityStatus = false;
            }

            await this.userRepo.save(foundUser);

            const savedTask = await this.taskRepo.save(newTask);

            tasksArray.push(savedTask);
        };

        return this.coreService.toProjectReturnDTO(savedProject)
    }


    async addResource(projectId: number, createResourceDTO: CreateResourceDTO, createTaskDTO: CreateTaskDTO): Promise<Project> {

        const foundSkill: Skill = await this.skillsRepo.findOne({
            where: {
                id: createResourceDTO.skillId
            }
        });

        const foundProject = await this.getProjectById(projectId);

        if (foundProject.resources.map(x => x.skill.name).includes(foundSkill.name)) {
            throw new PPSError(`Resource with skill ${foundSkill.name} already exists!`, 404);
        }

        const newResource = this.resourceRepo.create();
        newResource.skill = foundSkill;
        newResource.hours = createResourceDTO.hours;
        newResource.project = Promise.resolve(foundProject);

        const savedResource = await this.resourceRepo.save(newResource)

        const tasksArray = [];

        for (let i = 0; i < createTaskDTO.tasks.length; i++) {
            const newTask: Partial<Task> = this.taskRepo.create();

            const foundUser = await this.userRepo.findOne({
                where: {
                    id: createTaskDTO.tasks[i].userId,
                    availabilityStatus: true,
                    skill: foundSkill
                }
            });

            if (!foundUser) {
                throw new PPSError(`There is no user with id ${createTaskDTO.tasks[i].userId}!`, 404);
            }

            const foundProject = await this.getProjectById(projectId)

            newTask.hours = createTaskDTO.tasks[i].hours;
            newTask.skill = foundSkill;
            newTask.startDate = new Date();
            newTask.deleteDate = new Date();
            newTask.project = Promise.resolve(foundProject);
            newTask.user = Promise.resolve(foundUser);


            if (foundUser.availability - createTaskDTO.tasks[i].hours < 0) {
                throw new PPSError(`This employee is not a slave! His availability is less than ${createTaskDTO.tasks[i].hours}`, 404);
            }


            (await newTask.user).availability = (await newTask.user).availability - createTaskDTO.tasks[i].hours;

            if ((await newTask.user).availability === 0) {
                (await newTask.user).availabilityStatus = false;
            }

            await this.userRepo.save((await newTask.user));

            const savedTask = await this.taskRepo.save(newTask);

            tasksArray.push(savedTask);

        }

        return newResource.project;

        // this.addTasks(createTaskDTO, projectId);
    }


    async deleteResource(projectId: number, resourceId: number): Promise<Resource> {

        const foundResource = await this.resourceRepo.findOne({
            where: {
                id: resourceId
            }
        });

        const foundProject = await this.getProjectById(projectId);

        foundResource.isDeleted = true;
        const savedResource = await this.resourceRepo.save(foundResource);

        const foundTasks = await this.taskRepo.find({
            where: {
                skill: foundResource.skill,
                project: foundProject
            },
            relations: ['user']
        })

        const savedTasks = [];

        for (let i = 0; i < foundTasks.length; i++) {
            foundTasks[i].isDeleted = true;
            foundTasks[i].deleteDate = new Date();
            const taskUser = await foundTasks[i].user
            const foundUser = await this.userRepo.findOne({
                where: {
                    id: taskUser.id
                }
            });
            foundUser.availability += foundTasks[i].hours;
            if (foundUser.availability > 8) {
                foundUser.availability = 8;
            }

            const savedfoundUser = await this.userRepo.save(foundUser);
            console.log(foundUser)
            await this.taskRepo.save(foundTasks[i])

            savedTasks.push(foundTasks[i])
        }

        await this.taskRepo.save(savedTasks);

        return savedResource;
    }


    async updateResourceTime(resourceId: number, hours: number): Promise<Resource> {

        const foundResource = await this.resourceRepo.findOne({
            where: {
                id: resourceId
            }
        });

        foundResource.hours = hours;
        const savedResource = await this.resourceRepo.save(foundResource);

        return savedResource;
    }

    async getAllProjects(): Promise<ReturnProjectDTO[]> {
        const foundProjects = await this.projectRepo.find({
            order: { id: "DESC" },
            relations: ['tasks', 'resources', 'manager']
        });

        return foundProjects.map(project => this.coreService.toProjectReturnDTO(project));
    }


    async getUsersProjects(user: User): Promise<ReturnProjectDTO[]> {
        const foundProjects = await this.projectRepo.find({
            where: {
                manager: user,
            },
            order: { id: "DESC" },
            relations: ['tasks', 'resources']
        })

        return foundProjects.map(project => this.coreService.toProjectReturnDTO(project));
    }


    private async getProjectById(projectId: number): Promise<Project> {
        const foundProject: Project = await this.projectRepo.findOne({
            where: {
                id: projectId,
            },
            relations: ['resources', 'manager'],
            join: {
                alias: "project",
                leftJoinAndSelect: {
                    "tasks": "project.tasks",
                    "user": "tasks.user",
                    "skill": "tasks.skill",
                },
            }
            // relations: ['tasks', 'resources', 'manager'],
        })

        if (!foundProject) {
            throw new PPSError(`There is no user with id ${projectId}!`, 404);
        }

        await this.calculateTime(foundProject);

        return foundProject;
    }

    async getProjectByIdPublic(projectId: number): Promise<ReturnProjectDTO> {

        const foundProject = await this.getProjectById(projectId);

        return this.coreService.toProjectReturnDTO(foundProject);
    }


    async removeTask(taskId: number): Promise<Task> {

        const foundTask = await this.taskRepo.findOne({
            where: {
                id: taskId
            },
            relations: ['user']
        });

        if (!foundTask) {
            throw new PPSError(`There is no task with id ${taskId}!`, 404);
        }

        foundTask.deleteDate = new Date();
        foundTask.isDeleted = true;

        const foundUser = await this.userRepo.findOne({
            where: {
                id: (await foundTask.user).id,
            }
        });

        foundUser.availability += foundTask.hours;

        await this.userRepo.save(foundUser);
        const savedTask = await this.taskRepo.save(foundTask);

        return savedTask;

    }


    async addTasks(createTaskDTO: CreateTaskDTO, projectId: number): Promise<Project> {

        const tasksArray = [];

        for (let i = 0; i < createTaskDTO.tasks.length; i++) {
            const newTask: Partial<Task> = this.taskRepo.create();

            const currSkill = await this.skillsRepo.findOne({
                where: {
                    id: createTaskDTO.tasks[i].skillId
                }
            });

            const foundUser = await this.userRepo.findOne({
                where: {
                    id: createTaskDTO.tasks[i].userId,
                    availabilityStatus: true,
                    skill: currSkill
                }
            });

            if (!foundUser) {
                throw new PPSError(`There is no user with id ${createTaskDTO.tasks[i].userId}!`, 404);
            }

            const foundProject = await this.getProjectById(projectId)

            newTask.hours = createTaskDTO.tasks[i].hours;
            newTask.skill = currSkill;
            newTask.startDate = new Date();
            newTask.deleteDate = new Date();
            newTask.project = Promise.resolve(foundProject);
            newTask.user = Promise.resolve(foundUser);


            if (foundUser.availability - createTaskDTO.tasks[i].hours < 0) {
                throw new PPSError(`This employee is not a slave! His availability is less than ${createTaskDTO.tasks[i].hours}`, 404);
            }


            (await newTask.user).availability = (await newTask.user).availability - createTaskDTO.tasks[i].hours;

            // if ((await newTask.user).availability === 0) {
            //     (await newTask.user).availabilityStatus = false;
            // }

            await this.userRepo.save((await newTask.user));

            // const savedProject = await this.projectRepo.save(foundProject);
            const savedTask = await this.taskRepo.save(newTask);

            tasksArray.push(savedTask);

            return newTask.project;
        }
    }

    async calculateTime(project: Project) {

        const projectResources: Resource[] = project.resources;
        const timePerResourceArray = [];

        for (let i = 0; i < projectResources.length; i++) {

            const tasks = await this.taskRepo.find({
                where: {
                    isDeleted: false,
                    skill: projectResources[i].skill,
                    project
                }
            });

            const tasksDeleted = await this.taskRepo.find({
                where: {
                    isDeleted: true,
                    skill: projectResources[i].skill,
                    project
                }
            });

            // The code below is not working properly

            // const tasks: Task[] = (await project.tasks).filter(t => {
            //     if (t.skill === project.resources[i].skill && t.isDeleted === false) {
            //         return true;
            //     }
            //     return false;
            // });

            // const tasksDeleted: Task[] = (await project.tasks).filter(t => {
            //     if (t.skill === project.resources[i].skill && t.isDeleted === true) {
            //         return true;
            //     }
            //     return false;
            // });

            const dailyInputSum = tasks.reduce((sum, task) => {
                sum += task.hours;
                return sum;
            }, 0);

            const dailyInputSumForDeletedTasks = tasksDeleted.reduce((sum, task) => {
                sum += task.hours;
                return sum;
            }, 0);

            const daysPassedSinceProjectStart = ((new Date().getTime() - new Date(project.startDate).getTime()) / (60 * 60 * 24 * 1000));

            const totalDaysNeededPerResource = projectResources[i].hours / dailyInputSum;
            // console.log(totalDaysNeededPerResource);

            const workDoneInDaysForTasks = tasksDeleted.reduce((sum, task) => {

                const daysPassed = ((new Date(task.deleteDate).getTime() - new Date(task.startDate).getTime()) / (60 * 60 * 24 * 1000));
                const workDoneInDays = ((daysPassed * task.hours) / 24);

                sum += workDoneInDays;
                return sum;
            }, 0);

            const totalTimeInDaysPerResource = +((totalDaysNeededPerResource - daysPassedSinceProjectStart) - workDoneInDaysForTasks).toFixed(2);
            const leftTimeInHoursPerResource = totalTimeInDaysPerResource * dailyInputSum;

            timePerResourceArray.push(totalTimeInDaysPerResource);

        }

        return timePerResourceArray;
    }


    // update users availability when resource is completed
    @Cron('20 * * * * *')
    async updateUsersAvailability() {

        const foundProjects = await this.projectRepo.find({
            where: {
                status: ProjectStatus.inProgress
            }
        });

        // Deletion of completed resources
        foundProjects.map(project => {
            project.resources.map(async resource => {
                const leftTime = await this.calculateTimePerResource(project, resource);
                if (leftTime <= 0) {
                    await this.deleteResource(project.id, resource.id);
                }
            })
        })

        // Changing project status if all resources are completed
        for (let i = 0; i < foundProjects.length; i++) {
            const onGoingResources = foundProjects[i].resources.filter(r => r.isDeleted == false);
            if (onGoingResources.length === 0) {
                foundProjects[i].completeDate = new Date();
                foundProjects[i].status = ProjectStatus.completed;
                await this.projectRepo.save(foundProjects[i]);
            }
        }
    }

    async stopProject(projectId: number): Promise<Project> {

        const foundProject: Project = await this.getProjectById(projectId);
        console.log('DESI')
        const managementTask: Task[] = (foundProject as any).__tasks__.filter(t => t.skill.name === 'Management');
        console.log(managementTask[0]);
        const foundManagementTask = await this.taskRepo.findOne({
            where: {
                id: managementTask[0].id
            }
        });
        foundManagementTask.isDeleted = true;

        const savedMangementTask = await this.taskRepo.save(foundManagementTask);
        foundProject.status = ProjectStatus.stopped;
        foundProject.completeDate = new Date();

        

        const foundManager: User = await this.userRepo.findOne({
            where: {
                id:(await foundProject.manager).id
            },
            relations: ['tasks']
        });
        foundManager.availability += managementTask[0].hours;
        const savedManager = await this.userRepo.save(foundManager);
        console.log(savedManager.availability)
        

        foundProject.resources.forEach(r => this.deleteResource(projectId, r.id));
        const savedProject = await this.projectRepo.save(foundProject);

        return savedProject;
    }


    async calculateTimePerResource(project: Project, resource: Resource) {
        const projectTasks = await this.taskRepo.find({
            where: {
                project: project
            }
        });

        const tasks = projectTasks.filter(task => task.skill.name === resource.skill.name && task.isDeleted === false);
        const tasksHours = tasks.reduce((sum, task) => {
            sum += task.hours;
            return sum;
        }, 0);

        const tasksDeleted = projectTasks.filter(task => task.skill.name === resource.skill.name && task.isDeleted === true);
        const deletedTaskHours = tasksDeleted.reduce((sum, task) => {
            sum += task.hours;
            return sum;
        }, 0);



        const totalDaysNeededPerResource = resource.hours / tasksHours;
        const daysPassedSinceProjectStart = ((new Date().getTime() - new Date(project.startDate).getTime()) / (60 * 60 * 24 * 1000));

        const workDoneInDaysForDeletedTasks = tasksDeleted.reduce((sumInDays, task) => {
            // tslint:disable-next-line: max-line-length
            let daysPassedUntilTaskDeleted = new Date(task.deleteDate).getTime() - new Date(task.startDate).getTime();

            if (daysPassedUntilTaskDeleted > 0) {
                daysPassedUntilTaskDeleted = daysPassedUntilTaskDeleted / (60 * 60 * 24 * 1000);
            } else {
                daysPassedUntilTaskDeleted = 1;
            }

            const workDoneInDays = +((daysPassedUntilTaskDeleted * task.hours) / 24).toFixed(2);
            const workDoneInHours = workDoneInDays / task.hours;

            sumInDays += workDoneInDays;
            return +sumInDays.toFixed(2);
        }, 0);

        // tslint:disable-next-line: max-line-length
        const leftTimeInDaysPerResource = +(totalDaysNeededPerResource - daysPassedSinceProjectStart - workDoneInDaysForDeletedTasks).toFixed(2);
        // console.log(totalDaysNeededPerResource);
        const leftTimeInHoursPerResource = leftTimeInDaysPerResource * tasksHours;

        return Math.ceil(leftTimeInDaysPerResource);
    }

}
