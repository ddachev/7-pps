import { Resource } from "../../database/entities/resources.entity";
import { Task } from "../../database/entities/tasks.entity";
import { Skill } from "src/database/entities/skill.entity";

export class CreateProjectDTO {
    title: string;
    description: string;
    endDate: string;
    managerId: number;
    // skills: string[];
    resources: any[];
    tasks: any[];
}