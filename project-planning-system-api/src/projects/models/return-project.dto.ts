import { ReturnUserWithoutRelationsDTO } from "../../users/models/return-user-without-relations.dto";

export class ReturnProjectDTO {
    id: number;
    title: string;
    status: string;
    description: string;
    startDate: Date;
    endDate: Date;
    completeDate: Date;
    manager: ReturnUserWithoutRelationsDTO;
    resources: any[];
    tasks: any[];
}