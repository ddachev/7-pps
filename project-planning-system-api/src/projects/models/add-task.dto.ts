
export class CreateTaskDTO {
    tasks: [{
        hours: number,
		skillId: number,
		userId: number
    }];
}