import * as bcrypt from 'bcrypt'
import { Repository, In } from "typeorm";
import { createConnection } from "typeorm";
import { Role } from '../../database/entities/roles.entity';
import { UserRoles } from '../../common/enums/roles.enum';
import { User } from '../../database/entities/user.entity';
import { UserPositions } from '../../common/enums/positions.enum';
import { Skill } from '../../database/entities/skill.entity';

const seedRoles = async (connection: any) => {
    const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);
    const roles: Role[] = await rolesRepo.find();
    if (roles.length) {
        console.log('The DB already has roles!');
        return;
    }

    const rolesSeeding: Role[] = Object.keys(UserRoles).map(
        (roleName: string) => rolesRepo.create({ name: roleName })
    );

    await rolesRepo.save(rolesSeeding);
    console.log('Seeded roles successfully!');
};

const seedManagementSkill = async (connection: any) => {
    const skillsRepo: Repository<Skill> = connection.manager.getRepository(Skill);
    const skills: Skill[] = await skillsRepo.find();
    if (skills.length) {
        console.log('The DB already has skills!');
        return;
    }

    const newSkill = skillsRepo.create();
    newSkill.name = 'Management';

    await skillsRepo.save(newSkill);
    console.log('Management skill seeded successfully!');
};

const seedAdmin = async (connection: any) => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

    const admin = await userRepo.findOne({
        where: {
            name: 'admin',
        },
    });

    if (admin) {
        console.log('The DB already has an admin!');
        return;
    }

    const roleNames: string[] = Object.keys(UserRoles);
    const allUserRoles: Role[] = await rolesRepo.find({
        where: {
            name: In(roleNames),
        },
    });

    console.log(allUserRoles);

    if (allUserRoles.length === 0) {
        console.log('The DB does not have any roles!');
        return;
    }

    const password = 'Aaa123';
    const hashedPassword = await bcrypt.hash(password, 10);

    const adminProperties = {
        firstName: 'admin',
        middleName: 'admin',
        lastName: 'admin',
        email: 'admin@abv.bg',
        password: hashedPassword,
        position: UserPositions.administrator,
        role: allUserRoles[0]
    }

    const newAdmin: User = userRepo.create(adminProperties);

    await userRepo.save(newAdmin);
    console.log('Seeded admin successfully!');
};

const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();

    await seedRoles(connection);
    await seedManagementSkill(connection);
    await seedAdmin(connection);

    await connection.close();
    console.log('Seed completed!');
};

seed().catch(console.error);
